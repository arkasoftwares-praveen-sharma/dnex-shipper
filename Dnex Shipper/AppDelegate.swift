//
//  AppDelegate.swift
//  Dnex Shipper
//
//  Created by Arka on 05/02/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import Firebase
import UserNotifications




@available(iOS 13.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        
        GMSServices.provideAPIKey("AIzaSyDOCjcy-rgsNIbBNtcMXqfIIGxqtLTlAr8")
        
        FirebaseApp.configure()
        
        
        Messaging.messaging().delegate = self
        
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        
        application.registerForRemoteNotifications()
        
        
        let token = UserDefaults.standard.string(forKey: "sideIcon") ?? ""
        
        if token == ""{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "login")
            
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "Home")
            
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }
        
        return true
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs  can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        print(String(decoding: deviceToken, as: UTF8.self))
        
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                
                
                
                
                
                
            }
        }
        
        
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
    
     // when App is in Foreground
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        
        //        playSound()
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler(.alert)
    }
    
    // when App is in Background
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo as Dictionary<AnyHashable, Any>
        // Print message ID.
        
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        
        //        let UserIDPush:String = (userInfo["gcm.notification.user_id"] as? String)!
        //        let Type:String = (userInfo["gcm.notification.type"] as? String)!
        //        let profileImg:String = (userInfo["gcm.notification.avatar"] as? String)!
        //        let name:String = (userInfo["gcm.notification.name"] as? String)!
        
        
        //        UserDefaults.standard.set(UserIDPush, forKey: "userIdNotification") //setObject
        //        UserDefaults.standard.set(Type, forKey: "typeNotification") //setObject
        //        UserDefaults.standard.set(profileImg, forKey: "profileNotification") //setObject
        //        UserDefaults.standard.set(name, forKey: "nameNotification") //setObject
        //        UserDefaults.standard.set("push", forKey: "push") //setObject
        //
        //        print(Type)
        //
        //        if Type == "chat"{
        //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //            let initialViewController = storyboard.instantiateViewController(withIdentifier: "chatDetail")
        //
        //            self.window?.rootViewController = initialViewController
        //            self.window?.makeKeyAndVisible()
        //
        //        }
        //
        //
        //
        //        else{
        //            let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //            let initialViewController = storyboard.instantiateViewController(withIdentifier: "notification")
        //
        //            self.window?.rootViewController = initialViewController
        //            self.window?.makeKeyAndVisible()
        //        }
        
        completionHandler()
    }
    
}


@available(iOS 13.0, *)
extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        
        //        UserDefaults.standard.set(fcmToken, forKey: "Token") //setObject
        
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
    
    

}
