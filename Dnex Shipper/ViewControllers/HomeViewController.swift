//
//  HomeViewController.swift
//  Dnex Shipper
//
//  Created by Arka on 25/02/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON
import ADCountryPicker
import Firebase


struct item {
    var name = ""
    var description = ""
    var height = ""
    var weight = ""
    var width = ""
    var item_image = UIImage()
}



class HomeViewController: UIViewController,CLLocationManagerDelegate,GMSMapViewDelegate,ADCountryPickerDelegate,UITextFieldDelegate {

    
    @IBOutlet weak var passView: UIView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var submitBtnOutlet: UIButton!
    @IBOutlet weak var recenterOutlet: UIButton!
    
    
    @IBOutlet weak var onlineTitle: UILabel!
    @IBOutlet weak var onlineDesc: UILabel!
    @IBOutlet weak var onlineImg: UIButton!
    @IBOutlet weak var onlineView: UIView!
    
    @IBOutlet weak var senderImg: UIImageView!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var paymentType: UILabel!
    
    @IBOutlet weak var pickUpView: UIView!
    @IBOutlet weak var dropView: UIView!
    @IBOutlet weak var acceptOutlet: UIButton!
    
    @IBOutlet weak var customPickView: UIView!
    @IBOutlet weak var customDropView: UIView!
    @IBOutlet weak var itemView: UIView!
    
    @IBOutlet weak var receiveOrderView: UIView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var nameInitialsLbl: UILabel!
    
    
    @IBOutlet weak var customView: UIView!
    
    @IBOutlet weak var startBtnOutlet: UIButton!
    @IBOutlet weak var pickView: UIView!
    
    @IBOutlet weak var startView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var receiverView: UIView!
    
    
    @IBOutlet weak var callBtnOutlet: UIButton!
    @IBOutlet weak var cancelBtnOutlet: UIButton!
    
    @IBOutlet weak var cancelConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var mapTop: NSLayoutConstraint!
    @IBOutlet weak var startTop: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    
    @IBOutlet weak var pickUpLocationTxt: UILabel!
    @IBOutlet weak var receiverTxt: UILabel!
    
    @IBOutlet weak var itemReceiverName: UILabel!
    @IBOutlet weak var itemReceiverMobile: UILabel!
    
    @IBOutlet weak var itemTable: UITableView!
    
    @IBOutlet weak var sideBtn: UIButton!
    
    @IBOutlet weak var ProfileImg: UIButton!
    
    @IBOutlet weak var imageImg: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var addressTxt: UILabel!
    
    
    @IBOutlet weak var receiverInitial: UILabel!
    @IBOutlet weak var send: UIButton!
    @IBOutlet weak var tooFar: UIButton!
    @IBOutlet weak var mistake: UIButton!
    @IBOutlet weak var other: UIButton!
    
    @IBOutlet weak var messageTxt: ACFloatingTextfield!
    @IBOutlet weak var cancelHeight: NSLayoutConstraint!

    @IBOutlet weak var cancelOrderOutlet: UIButton!
    @IBOutlet weak var cancelOrderView: UIView!
    
    @IBOutlet weak var passDriverView: UIView!
    
    @IBOutlet weak var driver_code: ACFloatingTextfield!
    
    @IBOutlet weak var driver_mobile: ACFloatingTextfield!
    
    @IBOutlet weak var submitDriverBtnOutlet: UIButton!
    //    var orderList:[curr]
    var orders:[current] = []
    var flag = ""
    var Index = 0
    var newItemList:[item_order] = []
    var itemList:[item] = []
    let marker = GMSMarker()
    let destMarker = GMSMarker()
    
    let locationManager = CLLocationManager()
    var destination: CLLocationCoordinate2D = CLLocationCoordinate2D()
    var orderReceived = true
    var dragged = ""
    var orderId = ""
    var location = CLLocation()
    var previous_loc: CLLocationCoordinate2D = CLLocationCoordinate2D()
    
    var dummy = ""
    var TimerToFetchOrder = Timer()
    
    var order_status = ""
    
    var current_status = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        itemList.append(item.init(name: "chair", description: "Test description for chair", height: "20", weight: "10", width: "2", item_image: UIImage (named: "noPathCopy2")!))
//        itemList.append(item.init(name: "Table", description: "Test description for table", height: "20", weight: "10", width: "2", item_image: UIImage (named: "noPathCopy2")!))
//        itemList.append(item.init(name: "chair", description: "Test description for chair", height: "20", weight: "10", width: "2", item_image: UIImage (named: "noPathCopy2")!))
   
        
         GIFHUD.shared.setGif(named: "loader.gif")
        let device_token = Messaging.messaging().fcmToken ?? ""
        
        NetworkManager.update_device_id(uiRef: self, Webservice: "update-device-token", device_token: device_token){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            
        }
        
//        let countryDictionary = ["AF":"93", "AL":"355", "DZ":"213","AS":"1", "AD":"376", "AO":"244", "AI":"1","AG":"1","AR":"54","AM":"374","AW":"297","AU":"61","AT":"43","AZ":"994","BS":"1","BH":"973","BD":"880","BB":"1","BY":"375","BE":"32","BZ":"501","BJ":"229","BM":"1","BT":"975","BA":"387","BW":"267","BR":"55","IO":"246","BG":"359","BF":"226","BI":"257","KH":"855","CM":"237","CA":"1","CV":"238","KY":"345","CF":"236","TD":"235","CL":"56","CN":"86","CX":"61","CO":"57","KM":"269","CG":"242","CK":"682","CR":"506","HR":"385","CU":"53","CY":"537","CZ":"420","DK":"45","DJ":"253","DM":"1","DO":"1","EC":"593","EG":"20","SV":"503","GQ":"240","ER":"291","EE":"372","ET":"251","FO":"298","FJ":"679","FI":"358","FR":"33","GF":"594","PF":"689","GA":"241","GM":"220","GE":"995","DE":"49","GH":"233","GI":"350","GR":"30","GL":"299","GD":"1","GP":"590","GU":"1","GT":"502","GN":"224","GW":"245","GY":"595","HT":"509","HN":"504","HU":"36","IS":"354","IN":"91","ID":"62","IQ":"964","IE":"353","IL":"972","IT":"39","JM":"1","JP":"81","JO":"962","KZ":"77","KE":"254","KI":"686","KW":"965","KG":"996","LV":"371","LB":"961","LS":"266","LR":"231","LI":"423","LT":"370","LU":"352","MG":"261","MW":"265","MY":"60","MV":"960","ML":"223","MT":"356","MH":"692","MQ":"596","MR":"222","MU":"230","YT":"262","MX":"52","MC":"377","MN":"976","ME":"382","MS":"1","MA":"212","MM":"95","NA":"264","NR":"674","NP":"977","NL":"31","AN":"599","NC":"687","NZ":"64","NI":"505","NE":"227","NG":"234","NU":"683","NF":"672","MP":"1","NO":"47","OM":"968","PK":"92","PW":"680","PA":"507","PG":"675","PY":"595","PE":"51","PH":"63","PL":"48","PT":"351","PR":"1","QA":"974","RO":"40","RW":"250","WS":"685","SM":"378","SA":"966","SN":"221","RS":"381","SC":"248","SL":"232","SG":"65","SK":"421","SI":"386","SB":"677","ZA":"27","GS":"500","ES":"34","LK":"94","SD":"249","SR":"597","SZ":"268","SE":"46","CH":"41","TJ":"992","TH":"66","TG":"228","TK":"690","TO":"676","TT":"1","TN":"216","TR":"90","TM":"993","TC":"1","TV":"688","UG":"256","UA":"380","AE":"971","GB":"44","US":"1", "UY":"598","UZ":"998", "VU":"678", "WF":"681","YE":"967","ZM":"260","ZW":"263","BO":"591","BN":"673","CC":"61","CD":"243","CI":"225","FK":"500","GG":"44","VA":"379","HK":"852","IR":"98","IM":"44","JE":"44","KP":"850","KR":"82","LA":"856","LY":"218","MO":"853","MK":"389","FM":"691","MD":"373","MZ":"258","PS":"970","PN":"872","RE":"262","RU":"7","BL":"590","SH":"290","KN":"1","LC":"1","MF":"590","PM":"508","VC":"1","ST":"239","SO":"252","SJ":"47","SY":"963","TW":"886","TZ":"255","TL":"670","VE":"58","VN":"84","VG":"284","VI":"340"]
//
//
//        if let countryCodeGet = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
//            print(countryCodeGet)
//            driver_code.text = "+" + (countryDictionary[countryCodeGet] ?? "")
//        }
        
        driver_code.text = "+971"
//        driver_code.delegate = self
        driver_code.isEnabled = false
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            //            locationManager.startUpdatingLocation()
        }
        else{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Please enable the location services from device settings".localized)
        }
        
        submitDriverBtnOutlet.layer.cornerRadius = submitDriverBtnOutlet.frame.height/2
        
        receiveOrderView.isHidden = false
        cancelHeight.constant = 420
        let constantHeight:CGFloat = CGFloat((124 * itemList.count) + 300)
        bottomConstraint.constant = 0
        print(constantHeight)
        print(self.view.frame.size.height)
        
        cancelOrderOutlet.layer.cornerRadius = cancelOrderOutlet.frame.height/2
        
        if constantHeight > self.view.frame.size.height - 50{
            heightConstraint.constant = self.view.frame.size.height - 50
        }
        else{
            heightConstraint.constant = constantHeight
        }
        let image = UserDefaults.standard.string(forKey: "profile_pic") ?? ""
        print(image)
       
        self.ProfileImg.layer.cornerRadius = self.ProfileImg.frame.height/2
        self.ProfileImg.clipsToBounds = true
        
        
        if image != "http://103.207.168.164:8008/storage/"{

            URLSession.shared.dataTask(with: NSURL(string: image)! as URL, completionHandler: { (data, response, error) -> Void in
            
            if error != nil {
                print(error ?? "No Error")
                return
            }
            DispatchQueue.main.async(execute: { () -> Void in
                let image = UIImage(data: data!)
                //                activityIndicator.removeFromSuperview()
                self.ProfileImg.setBackgroundImage(image, for: .normal)
                
            })
            
        }).resume()
        
        
    }
        else{
            self.ProfileImg.setBackgroundImage(UIImage (named: "defaultProfile"), for: .normal)

        }
        
//        destination.latitude = 26.906954
//        destination.longitude = 75.7199461
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        // Do any additional setup after loading the view.
        headerView.setShadow()
        mapView.delegate = self
//        submitBtnOutlet.layer.cornerRadius = submitBtnOutlet.frame.height/2
//        mapView.isMyLocationEnabled = true
//        mapView.settings.myLocationButton = true
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
//        locationManager.startUpdatingHeading()
        
//        destMarker.position = destination
//        destMarker.map = mapView
        recenterOutlet.isHidden = true
        
        mapView.addSubview(recenterOutlet)
//        mapView.addSubview(submitBtnOutlet)
//         mapView.addSubview(startView)
        
        senderImg.layer.cornerRadius = senderImg.frame.height/2
        nameInitialsLbl.layer.cornerRadius = nameInitialsLbl.frame.height/2
        nameInitialsLbl.clipsToBounds = true
        pickUpView.layer.cornerRadius = pickUpView.frame.height/2
        dropView.layer.cornerRadius = dropView.frame.height/2
        startBtnOutlet.layer.cornerRadius = startBtnOutlet.frame.height/2
        pickView.layer.cornerRadius = pickView.frame.height/2
        
        receiverView.layer.cornerRadius = 5
        receiverView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        receiverView.layer.borderWidth = 1
        
        pickUpView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        pickUpView.layer.borderWidth = 1
        
        dropView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        dropView.layer.borderWidth = 1
        
        senderImg.layer.borderColor = UIColor().HexToColor(hexString: "00BAE3").cgColor
        senderImg.layer.borderWidth = 1
        
        customDropView.layer.cornerRadius = customDropView.frame.height/2
        customPickView.layer.cornerRadius = customPickView.frame.height/2
        
        acceptOutlet.layer.cornerRadius = acceptOutlet.frame.height/2
        
        UserDefaults.standard.set("home", forKey: "sideIcon")
        
        
        
        /// Check Online status
        
        NetworkManager.checkOnlineStatus(uiRef: self, Webservice: "shipper-online-status"){sJson in
            
            let sJSON = JSON(sJson)
            
            print(sJSON)
            
            if sJSON["status"].intValue == 200{
                
                if sJSON["data"]["online"].intValue == 0{

                    self.onlineTitle.text = "You are Offline".localized
                    self.onlineTitle.textColor = UIColor().HexToColor(hexString: "db2f2f")
                    
                    self.onlineView.backgroundColor = UIColor().HexToColor(hexString: "db2f2f")
                    
                    self.onlineDesc.text = "Tap to go online to start accepting orders".localized
                    
                    self.onlineImg.setImage(UIImage (named: "offlineIcon"), for: .normal)
                    
                }
                else{
                    
                    self.onlineTitle.text = "You are Online".localized
                    self.onlineTitle.textColor = UIColor().HexToColor(hexString: "18D293")
                    
                    self.onlineView.backgroundColor = UIColor().HexToColor(hexString: "18D293")
                    
                    self.onlineDesc.text = "Tap to go offline to stop delivery orders".localized
                    
                    self.onlineImg.setImage(UIImage (named: "onlineIcon"), for: .normal)
                    self.fetchOrder()
                }
            }
                
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
        }
        
        if self.revealViewController() != nil {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            sideBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.revealViewController().rearViewRevealWidth = self.view.frame.width
            
            sideBtn.setImage(UIImage (named: "sidebar"), for: .normal)
            
        }
        else{
            sideBtn.addTarget(self, action: #selector(back), for: .touchUpInside)
            sideBtn.setImage(UIImage (named: "backBlue"), for: .normal)
            
             bottomConstraint.constant = -245
        }
        
        
        if flag == "status"{
            
            //            if newStatus == "3"{
            //                self.orderArr.append("In Transit")
            //            }
            //            else if newStatus == "1"{
            //                self.orderArr.append("Pending")
            //            }
            //            else {
            //                self.orderArr.append("Picked Up")
            
            
            
            
            
            
            self.startTop.isHidden = false
            
            let constantHeight:CGFloat = CGFloat((124 * self.newItemList.count) + 300)
            print(constantHeight)
            
            if constantHeight > self.view.frame.size.height - 50{
                self.heightConstraint.constant = self.view.frame.size.height - 50
            }
            else{
                self.heightConstraint.constant = constantHeight
            }
            
            print(order_status)
            
//            if order_status == "Pending"{
//
//                nameLbl.text = orders[Index].sender_name
//                addressTxt.text = orders[Index].pickUp
//                itemReceiverName.text = orders[Index].receiver_name
//                itemReceiverMobile.text = orders[Index].receiver_mobile_code + orders[Index].receiver_mobile
//
//                let initials = itemReceiverName.text!.components(separatedBy: " ").reduce("") { ($0 == "" ? "" : "\($0.first!)") + "\($1.first!)" }
//
//                nameInitialsLbl.text = initials
//
//                let sender_image = orders[Index].sender_image
//                self.imageImg.sd_setImage(with: URL(string: sender_image), placeholderImage: UIImage(named: "defaultProfile"))
//
//                self.itemTable.reloadData()
//
//
//                let SenderLat = Double(orders[Index].sender_lat)
//                let SenderLong = Double(orders[Index].sender_long)
//
//                destination.latitude = SenderLat ?? 0.0
//                destination.longitude = SenderLong ?? 0.0
//
//
//                destMarker.position = destination
//                destMarker.map = mapView
//
//
//            }
            
            print(order_status)
            
            if order_status == "Pending"{
              
                order_status = "Picked Up"
                nameLbl.text = orders[Index].sender_name
                addressTxt.text = orders[Index].pickUp
                itemReceiverName.text = orders[Index].receiver_name
                itemReceiverMobile.text = orders[Index].receiver_mobile_code + orders[Index].receiver_mobile
                
                let initials = itemReceiverName.text!.components(separatedBy: " ").reduce("") { ($0 == "" ? "" : "\($0.first!)") + "\($1.first!)" }
                
                nameInitialsLbl.text = initials
                
                let sender_image = orders[Index].sender_image
                self.imageImg.sd_setImage(with: URL(string: sender_image), placeholderImage: UIImage(named: "defaultProfile"))
                
                self.itemTable.reloadData()
                
                
                let SenderLat = Double(orders[Index].sender_lat)
                let SenderLong = Double(orders[Index].sender_long)
                
                destination.latitude = SenderLat ?? 0.0
                destination.longitude = SenderLong ?? 0.0
                
                
                destMarker.position = destination
                destMarker.map = mapView
                
                
                
                mapView.animate(toZoom: 16)
                mapView.animate(toLocation: marker.position)
                
                startBtnOutlet.setTitle("PICK UP".localized, for: .normal)
                titleLbl.text = "PICK UP".localized
//                cancelConstraint.constant = -40
                cancelBtnOutlet.isHidden = true
            }
            else if order_status == "Picked Up"{
                startBtnOutlet.setTitle("COMPLETE".localized, for: .normal)
                titleLbl.text = "ON THE WAY".localized
                order_status = "completed"
                //            cancelConstraint.constant = -40
                //            cancelBtnOutlet.isHidden = true
                
                print(orders[Index].receiver_name)
                
                nameLbl.text = orders[Index].receiver_name
                addressTxt.text = orders[Index].receiver_address
                itemReceiverName.text = orders[Index].receiver_name
                itemReceiverMobile.text = orders[Index].receiver_mobile_code + orders[Index].receiver_mobile
                
                let initials = itemReceiverName.text!.components(separatedBy: " ").reduce("") { ($0 == "" ? "" : "\($0.first!)") + "\($1.first!)" }
                
                nameInitialsLbl.text = initials
                
                let sender_image = orders[Index].sender_image
                self.imageImg.sd_setImage(with: URL(string: sender_image), placeholderImage: UIImage(named: "defaultProfile"))
                
                self.itemTable.reloadData()
                mapView.clear()
                
                marker.position = location.coordinate
                marker.rotation = locationManager.location?.course ?? 0
                marker.icon = UIImage (named: "truck")
                marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                marker.map = mapView
                
                let ReceiverLat = Double(orders[Index].receiver_lat)
                let ReceiverLong = Double(orders[Index].receiver_long)
                
                destination.latitude = ReceiverLat ?? 0.0
                destination.longitude = ReceiverLong ?? 0.0
                
                
                destMarker.position = destination
                destMarker.map = mapView
                drawPath()
                
                
                //            let sender_image = orders[Index].recei
                //            self.imageImg.sd_setImage(with: URL(string: sender_image), placeholderImage: UIImage(named: "defaultProfile"))
                
                self.imageImg.image = UIImage (named: "defaultProfile")
            }
            
        }
        
        imageImg.layer.cornerRadius = imageImg.frame.height/2
     
    }
    
    @objc func back(){
        NotificationCenter.default.post(name: .cart, object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        
        // if GoogleMap installed
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            
            if let url = URL(string: "comgooglemaps://?saddr=&daddr=\(marker.position.latitude),\(marker.position.longitude)&directionsmode=driving") {
                UIApplication.shared.open(url, options: [:])
            }
            
        } else {
            
            //Open in browser
            if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(marker.position.latitude),\(marker.position.longitude)&directionsmode=driving") {
                UIApplication.shared.open(urlDestination)
            }
            
        }
        return true
    }
   
    @IBAction func closeDriverPass(_ sender: Any) {
        
        self.customView.isHidden = true
        self.passDriverView.isHidden = true
        driver_mobile.text = ""
        
    }
    
    @IBAction func passBtnClicked(_ sender: Any) {
        
        self.customView.isHidden = false
        self.passDriverView.isHidden = false
        
        if receiveOrderView.isHidden == false{
           receiveOrderView.isHidden = true
        }
        
        self.customView.alpha = 0.0
        self.passDriverView.alpha = 0.0
        
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            
            self.customView.alpha = 1.0
            self.passDriverView.alpha = 1.0
            
        })
        
    }
    
    
    @IBAction func submitDriverBtnClicked(_ sender: Any) {
        if driver_mobile.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter driver's mobile number !!".localized)
        }
        else{
            
             GIFHUD.shared.show(withOverlay: true, duration: 2)
            
            let order = orders[Index].id
            NetworkManager.pass_order(uiRef: self, Webservice: "pass_order", mobile: driver_mobile.text!, order_id: order){sJson in
                let sJSON = JSON(sJson)
                print(sJSON)
                CommonFunctions.delay(1.0) {
                    if sJSON["status"].intValue == 200{
                        self.showAlertPass(message: "Order sent to the driver, It will be passed as soon as the driver accepts the order".localized)
                    }
                    else{
                        CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                    }
                }
            }
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
         self.TimerToFetchOrder.invalidate()
    }
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        print(name)
        driver_code.text = dialCode
        self.dismiss(animated: true, completion: nil)
    }
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == driver_code{
//            driver_code.endEditing(true)
//            let picker = ADCountryPicker()
//            picker.delegate = self
//
//            let pickerNavigationController = UINavigationController(rootViewController: picker)
//            self.present(pickerNavigationController, animated: true, completion: nil)
//        }
//    }

    
    
    
    func fetchOrder(){
        
        TimerToFetchOrder = Timer.scheduledTimer(withTimeInterval: 4.0, repeats: true, block: { (_) in
             print("TIMER")
             
            let status = self.onlineTitle.text
            
             DispatchQueue.global(qos: .background).async {
                 print("This is run on the background queue")
                 
                 if status == "You are Online"{
                     
                     NetworkManager.getOrders(uiRef: self, Webservice: "get-order-request"){sJson in
                         
                         let sJSON = JSON(sJson)
                         
                         print(sJSON)
                         
                         if sJSON["status"].intValue == 200{
                                                        
                              for item1 in sJSON["data"]["get_order_details"]["get_product_details"].arrayValue{
                              
                                let itemName = item1["name"].stringValue
                                let itemDesc = item1["description"].stringValue
                                let itemHeight = item1["height"].stringValue
                                let itemWeight = item1["weight"].stringValue
                                let itemWidth = item1["width"].stringValue
                                
                                var imageStr = item1["image"].stringValue
                                
//                                let url = URL(string: imageStr.replacingOccurrences(of: "\\", with: ""))
                                
                                let newString = imageStr.replacingOccurrences(of: "\\", with: "")
                                
                                let BaseurlStr = "http://103.207.168.164:8008/storage/"
                                imageStr = BaseurlStr + newString
//
//                                let testImgView = UIImageView()
//                                testImgView.downloadImage(from: URL(string: imageStr)!)
//
//
//                                let image = testImgView.image
                                    
//                                let url = URL(string: "http://www.google.com")
                                
                                let url : String = imageStr
                                let urlStr : String = url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                                let convertedURL : URL = URL(string: urlStr)!
                                print(convertedURL)
                                
                                let data = try? Data(contentsOf: convertedURL)

                                var image = UIImage()
                                if let imageData = data {
                                    image = UIImage(data: imageData)!
                                }
                                                                
                                self.itemList.append(item.init(name: itemName, description: itemDesc, height: "\(itemHeight) ft", weight: "\(itemWeight) kg", width: "\(itemWidth) ft", item_image: image))
                            }
                            
                            let constantHeight:CGFloat = CGFloat((124 * self.itemList.count) + 300)
                            print(constantHeight)
                            
                            if constantHeight > self.view.frame.size.height - 50{
                                self.heightConstraint.constant = self.view.frame.size.height - 50
                            }
                            else{
                                self.heightConstraint.constant = constantHeight
                            }
                            
                            self.itemTable.reloadData()
                            
                            self.senderName.text = sJSON["data"]["get_order_details"]["get_user_details"]["name"].stringValue
                            self.paymentType.text = sJSON["data"]["get_order_details"]["get_payment_detail"]["payment_type"].stringValue
                            
                            self.orderId = sJSON["data"]["order_id"].stringValue
                            
                            self.pickUpLocationTxt.text = sJSON["data"]["get_order_details"]["pickup_address"].stringValue
                            self.receiverTxt.text = sJSON["data"]["get_order_details"]["receiver_address"].stringValue
                            
                            
                            self.itemReceiverName.text = sJSON["data"]["get_order_details"]["receiver_name"].stringValue
                            self.itemReceiverMobile.text = sJSON["data"]["get_order_details"]["receiver_mobile"].stringValue
                            
                            self.nameInitialsLbl.text = String(self.itemReceiverName.text!.prefix(1)) // result = "Sampl"
//
                            var imageStr = sJSON["data"]["get_order_details"]["get_user_details"]["profile_pic"].stringValue

                            let newString = imageStr.replacingOccurrences(of: "\\", with: "")
                            
                            let BaseurlStr = "http://103.207.168.164:8008/storage/"
                            imageStr = BaseurlStr + newString
                            
                            self.senderImg.sd_setImage(with: URL(string: imageStr), placeholderImage: UIImage(named: "defaultProfile"))
                            
                            self.TimerToFetchOrder.invalidate()
                            self.receiveOrderView.isHidden = false
                            self.customView.isHidden = false
                            
                         }
                         else{
//                             CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                         }
                     }
                 }
                 else{
                     self.TimerToFetchOrder.invalidate()
                 }
                 
                 DispatchQueue.main.async {
                     print("This is run on the main queue, after the previous code in outer block")
                 }
             }
             
         })
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      
        previous_loc = location.coordinate
        location = locations.last!
        
        //            let marker = GMSMarker()
        
        
        marker.position = location.coordinate
//        marker.rotation = locationManager.location?.course ?? 0
        marker.icon = UIImage (named: "truck")
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        self.marker.rotation = CLLocationDegrees(self.getHeadingForDirection(fromCoordinate: self.previous_loc, toCoordinate: location.coordinate))
        marker.map = mapView
        
        
        if dragged == ""{
            //          mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 16, bearing: 0, viewingAngle: 10)
            
//            if titleLbl.text == "PICK UP"{
//                mapView.animate(toZoom: 16)
//                mapView.animate(toLocation: marker.position)
//            }
//            else{
//                let bounds = GMSCoordinateBounds(coordinate: self.marker.position, coordinate: self.destMarker.position)
//                let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50.0)
//                self.mapView.animate(with: cameraWithPadding)
//            }
            
            if titleLbl.text == "PICK UP" || flag == ""{
                mapView.animate(toZoom: 16)
                mapView.animate(toLocation: marker.position)
            }
            else if titleLbl.text == "START"{
                var bounds = GMSCoordinateBounds()
                bounds = bounds.includingCoordinate(marker.position)
                bounds = bounds.includingCoordinate(destMarker.position)
                //                                let camera: GMSCameraUpdate = GMSCameraUpdate.fit(bounds)
                let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50)
                
                let camera = mapView.camera(for: bounds, insets: UIEdgeInsets())!

                mapView.camera = camera

                mapView.moveCamera(cameraWithPadding)
                
//                self.mapView.animate(with: camera)
                
//                mapView.camera = marker.position
                
            }
            
            
        }
        //        else{
        //            locationManager.stopUpdatingLocation()
        //        }
        
//        print("LOCATION \(location.coordinate)")
        
        if flag == "status" && dummy == ""{
            drawPath()
            dummy = "true"
        }
        
        
        /// Webservice to update Location
        
         let status = self.onlineTitle.text
        
        DispatchQueue.global(qos: .background).async {
            print("This is run on the background queue")

            if status == "You are Online"{
                
                NetworkManager.shareLocation(uiRef: self, Webservice: "update-shipper-location",latitude: self.location.coordinate.latitude,longitude: self.location.coordinate.longitude){sJson in
                    
                    let sJSON = JSON(sJson)
                    
                    print(sJSON)
                    
                    if sJSON["status"].intValue == 200{
                        
                    }
                    else{
//                        CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                    }
                }
            }
            
            DispatchQueue.main.async {
                print("This is run on the main queue, after the previous code in outer block")
            }
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        if (gesture){
            print("dragged")
            recenterOutlet.isHidden = false
            dragged = "yes"
        }
    }
   
    @IBAction func cancelBtnClicked(_ sender: Any) {
        
        self.cancelOrderView.isHidden = true
        self.customView.isHidden = true
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func callBtnAction(_ sender: Any) {
        
        
        var mobile_number = ""
        
        if startBtnOutlet.title(for: .normal) == "START"{
            mobile_number = orders[Index].sender_mobile_code + orders[Index].sender_mobile

        }
        else{
            mobile_number = orders[Index].receiver_mobile_code + orders[Index].receiver_mobile
        }
         
      guard let number = URL(string: "tel://" + mobile_number) else { return }
        UIApplication.shared.open(number)
        
    }
    
    
    @IBAction func cancelBtnAction(_ sender: Any) {
        
        self.customView.isHidden = false
        self.cancelOrderView.isHidden = false
        
        self.customView.alpha = 0.0
        self.cancelOrderView.alpha = 0.0
        
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
         
            self.customView.alpha = 1.0
            self.cancelOrderView.alpha = 1.0
            
        })
       
        
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
           customView.isHidden = true
        self.cancelOrderView.isHidden = true
        
        messageTxt.text = ""
        send.setImage(UIImage (named: "radioselect"), for: .normal)
        tooFar.setImage(UIImage (named: "radio"), for: .normal)
        mistake.setImage(UIImage (named: "radio"), for: .normal)
        other.setImage(UIImage (named: "radio"), for: .normal)
        
        messageTxt.isHidden = true
        cancelHeight.constant = 420
        
       }
    
    @IBAction func backBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func dontSend(_ sender: Any) {

          send.setImage(UIImage (named: "radioselect"), for: .normal)
          tooFar.setImage(UIImage (named: "radio"), for: .normal)
          mistake.setImage(UIImage (named: "radio"), for: .normal)
          other.setImage(UIImage (named: "radio"), for: .normal)

          messageTxt.isHidden = true
          cancelHeight.constant = 420
          
      }
      
      @IBAction func tooFar(_ sender: Any) {
          
          send.setImage(UIImage (named: "radio"), for: .normal)
          tooFar.setImage(UIImage (named: "radioselect"), for: .normal)
          mistake.setImage(UIImage (named: "radio"), for: .normal)
          other.setImage(UIImage (named: "radio"), for: .normal)
          
          messageTxt.isHidden = true
          cancelHeight.constant = 420
          
      }
      
      @IBAction func mistakeBtn(_ sender: Any) {
          
          send.setImage(UIImage (named: "radio"), for: .normal)
          tooFar.setImage(UIImage (named: "radio"), for: .normal)
          mistake.setImage(UIImage (named: "radioselect"), for: .normal)
          other.setImage(UIImage (named: "radio"), for: .normal)
          
          messageTxt.isHidden = true
          cancelHeight.constant = 420
          
      }
      
      @IBAction func otherBtn(_ sender: Any) {
          
          send.setImage(UIImage (named: "radio"), for: .normal)
          tooFar.setImage(UIImage (named: "radio"), for: .normal)
          mistake.setImage(UIImage (named: "radio"), for: .normal)
          other.setImage(UIImage (named: "radioselect"), for: .normal)
          
          messageTxt.isHidden = false
          cancelHeight.constant = 490
          
      }
    
    
    @IBAction func onlineBtnClicked(_ sender: Any) {
        
        if onlineTitle.text == "You are Online"{
            
            setOnlineStatus(online: "0")
            
        }
        else{
            setOnlineStatus(online: "1")
            
        }
    }
    
    
    func setOnlineStatus(online:String){
        
        NetworkManager.onlineStatus(uiRef: self, Webservice: "update-shipper-online-status", online_status: online){sJson in
            
            let sJSON = JSON(sJson)
            
            print(sJSON)
            
            if sJSON["status"].intValue == 200{
                if self.onlineTitle.text == "You are Online".localized{
                    
                    self.onlineTitle.text = "You are Offline".localized
                    self.onlineTitle.textColor = UIColor().HexToColor(hexString: "db2f2f")
                    
                    self.onlineView.backgroundColor = UIColor().HexToColor(hexString: "db2f2f")
                    
                    self.onlineDesc.text = "Tap to go online to start accepting orders".localized
                    
                    self.onlineImg.setImage(UIImage (named: "offlineIcon"), for: .normal)
                    
                    self.TimerToFetchOrder.invalidate()
                }
                else{
                    
                    self.onlineTitle.text = "You are Online".localized
                    self.onlineTitle.textColor = UIColor().HexToColor(hexString: "18D293")
                    
                    self.onlineView.backgroundColor = UIColor().HexToColor(hexString: "18D293")
                    
                    self.onlineDesc.text = "Tap to go offline to stop delivery orders".localized
                    
                    self.onlineImg.setImage(UIImage (named: "onlineIcon"), for: .normal)
                    
                    self.fetchOrder()

                }
            }
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
        }
    }

   
    
    func drawPath()
    {
        let origin = "\(marker.position.latitude),\(marker.position.longitude)"
        let destination = "\(destMarker.position.latitude),\(destMarker.position.longitude)"
        
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyCJxfZpF3sQo5PrBiLCObqPojJFR7W_DfU"
        
        //        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyDOCjcy-rgsNIbBNtcMXqfIIGxqtLTlAr8"
        print(url)
        let task = URLSession.shared.dataTask(with: URL(string: url)!) { (data, response, error) -> Void in
            
            do {
                if data != nil {
                    let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as!  [String:AnyObject]
                    print(dic)
                    
                    let status = dic["status"] as! String
                    var routesArray:String!
                    if status == "OK" {
                        routesArray = (((dic["routes"]!as! [Any])[0] as! [String:Any])["overview_polyline"] as! [String:Any])["points"] as! String
                        //                            print("routesArray: \(String(describing: routesArray))")
                        DispatchQueue.main.async {
                            
                            let path = GMSPath.init(fromEncodedPath: routesArray!)
                            let singleLine = GMSPolyline.init(path: path)
                            singleLine.strokeWidth = 2.0
                            singleLine.strokeColor = UIColor().HexToColor(hexString: "00BAE3")
                            singleLine.map = self.mapView
                            
                            if self.dragged == ""{
                                 var bounds = GMSCoordinateBounds()
                                bounds = bounds.includingCoordinate(self.marker.position)
                                bounds = bounds.includingCoordinate(self.destMarker.position)
                                //                                let camera: GMSCameraUpdate = GMSCameraUpdate.fit(bounds)
                                let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50.0)
                                
                                self.mapView.animate(with: cameraWithPadding)
                            }
                        }
                    }
                }
            } catch {
                print("Error")
            }
        }
        
        task.resume()
    }
    
    @IBAction func recenterBtnClicked(_ sender: Any) {
        
        recenterOutlet.isHidden = true
//
//        let bounds = GMSCoordinateBounds(coordinate: self.marker.position, coordinate: self.destMarker.position)
//        //                                let camera: GMSCameraUpdate = GMSCameraUpdate.fit(bounds)
//        let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50.0)
//
//        self.mapView.animate(with: cameraWithPadding)
        dragged = ""
        
//        locationManager.startUpdatingLocation()
        if titleLbl.text == "PICK UP" || flag == ""{
            mapView.animate(toLocation: marker.position)
            mapView.animate(toZoom: 16)
            
//            var bounds = GMSCoordinateBounds()
//            bounds = bounds.includingCoordinate(marker.position)
//            let camera = mapView.camera(for: bounds, insets: UIEdgeInsets())!
//
//             mapView.camera = camera
//
//            let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 500.0)
//            mapView.moveCamera(cameraWithPadding)
            
            
//             let bounds = GMSCoordinateBounds(coordinate: self.marker.position)
//                        //                                let camera: GMSCameraUpdate = GMSCameraUpdate.fit(bounds)
//            //            let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50.0)
//            //
//            //
//            //
//            //            self.mapView.animate(with: cameraWithPadding)
//            //            let newCamera = mapView.
//                        let camera = mapView.camera(for: bounds, insets: UIEdgeInsets())!
//                        mapView.camera = camera
//
            
        }
        else{
            let bounds = GMSCoordinateBounds(coordinate: self.marker.position, coordinate: self.destMarker.position)
            //                                let camera: GMSCameraUpdate = GMSCameraUpdate.fit(bounds)
//            let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50.0)
//
//
//
//            self.mapView.animate(with: cameraWithPadding)
//            let newCamera = mapView.
            let camera = mapView.camera(for: bounds, insets: UIEdgeInsets())!
            mapView.camera = camera
            
            let cameraWithPadding: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50.0)
            mapView.moveCamera(cameraWithPadding)
            
        }
    }
    
    @IBAction func closeItemBtnClicked(_ sender: Any) {
        
        self.itemView.alpha = 1.0
        //          forgotEmailTxt.text = ""
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.itemView.alpha = 0.0
            self.itemView.isHidden = true
            
            if self.orderReceived == true{
                self.receiveOrderView.isHidden = false
            }
            else{
                self.customView.isHidden = true
            }
           
        })
//        self.itemView.isHidden = true
    }
    
    
    @IBAction func itemInfoBtnClicked(_ sender: Any) {
//
//        if itemList.count == 0 || newItemList.count == 0{
//
//            CommonFunctions.showErrorAlert(uiRef: self, message :"Item description is not available !!")
//
//        }
//        else{
            
            self.itemView.isHidden = false
            self.receiveOrderView.isHidden = true
            self.orderReceived = true
            self.itemView.alpha = 0.0
            //          forgotEmailTxt.text = ""
            UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                self.itemView.alpha = 1.0
            })
//        }
    }
    
    @IBAction func acceptBtnClicked(_ sender: Any) {
        TimerToFetchOrder.invalidate()
        NetworkManager.accept(uiRef: self, Webservice: "accept-reject-order", order_id: orderId,status: "1"){sJson in
            
            
            let sJSON = JSON(sJson)
            print(sJSON)
            
            if sJSON["status"].intValue == 200{
                
                self.receiveOrderView.isHidden = true
                self.orderReceived = false
                self.customView.isHidden = true
                self.fetchOrder()
                
            }
                
            else if sJSON["status"].intValue == 400{
                
                self.showAlert(message: sJSON["message"].stringValue)
            }
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
        }
    }
    
    
    func showAlert( message: String) {
        let alertController = UIAlertController(title: "Message".localized, message:
               message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: {action in
              
            self.orderReceived = false
            self.customView.isHidden = true
            self.fetchOrder()
               
           }))
                  
           self.present(alertController, animated: true, completion: nil)
           
       }
    
    
    func showAlertPass( message: String) {
           let alertController = UIAlertController(title: "Message".localized, message:
               message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: {action in
            self.performSegue(withIdentifier: "Home", sender: nil)
//            self.dismiss(animated: true, completion: nil)
            
        }))
                  
           self.present(alertController, animated: true, completion: nil)
           
       }

    
    
    @IBAction func itemStartBtn(_ sender: Any) {
        
        self.itemView.isHidden = false
        self.customView.isHidden = false
        self.receiveOrderView.isHidden = true
        self.orderReceived = false
        self.itemView.alpha = 0.0
//      forgotEmailTxt.text = ""
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.itemView.alpha = 1.0
        })
    }
    
    @IBAction func rejectBtnClicked(_ sender: Any) {
        
         TimerToFetchOrder.invalidate()
        NetworkManager.accept(uiRef: self, Webservice: "accept-reject-order", order_id: orderId,status: "2"){sJson in
            
            
            let sJSON = JSON(sJson)
            print(sJSON)
            
            if sJSON["status"].intValue == 200{
                
                
                //        receiveOrderView.isHidden = true
                self.orderReceived = false
                self.customView.isHidden = true
                self.fetchOrder()
                
            }
            else if sJSON["status"].intValue == 400{
                
                self.showAlert(message: sJSON["message"].stringValue)
            }
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
            
        }
        
    }
    
    
    @IBAction func startBtnclicked(_ sender: Any) {

        var order_status_number = 0
        print(order_status)
        
        
        if self.order_status == "Pending"{
           self.order_status = "Picked Up"
//            drawPath()

//            let camera = GMSCameraPosition.camera(withLatitude: marker.position.latitude, longitude: marker.position.latitude, zoom: 10)
//            mapView.animate(to: camera)
            mapView.animate(toZoom: 16)
            mapView.animate(toLocation: marker.position)
            
            startBtnOutlet.setTitle("PICK UP".localized, for: .normal)
            titleLbl.text = "PICK UP".localized
//            cancelConstraint.constant = -40
            cancelBtnOutlet.isHidden = true
            
            order_status_number = 1
            
        }
        else if self.order_status == "Picked Up"{
            self.order_status = "completed"
            startBtnOutlet.setTitle("COMPLETE".localized, for: .normal)
            titleLbl.text = "ON THE WAY".localized
//            cancelConstraint.constant = -40
//            cancelBtnOutlet.isHidden = true
            
            print(orders[Index].receiver_name)
            passView.isHidden = true
            nameLbl.text = orders[Index].receiver_name
            addressTxt.text = orders[Index].receiver_address
            
            mapView.clear()
                        
            marker.position = location.coordinate
            marker.rotation = locationManager.location?.course ?? 0
            marker.icon = UIImage (named: "truck")
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            marker.map = mapView
            
            let ReceiverLat = Double(orders[Index].receiver_lat)
            let ReceiverLong = Double(orders[Index].receiver_long)
            
            destination.latitude = ReceiverLat ?? 0.0
            destination.longitude = ReceiverLong ?? 0.0
            
            
            destMarker.position = destination
            destMarker.map = mapView
            drawPath()
            
            
//            let sender_image = orders[Index].recei
//            self.imageImg.sd_setImage(with: URL(string: sender_image), placeholderImage: UIImage(named: "defaultProfile"))
            
            self.imageImg.image = UIImage (named: "defaultProfile")
            
            order_status_number = 2
            
        }
        else if self.order_status == "completed"{
            titleLbl.text = "COMPLETED".localized
            
             order_status_number = 5

        }
        
        orderId = orders[Index].id
        
        NetworkManager.updateStatus(uiRef: self, Webservice: "update-order-status", order_id: orderId, order_status: order_status_number){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            
            if sJSON["status"].intValue == 200{
//                self.showAlertSuccess(message: "Password updated successfully !!")
                if self.titleLbl.text == "COMPLETED".localized{
                    
                    self.performSegue(withIdentifier: "receipt", sender: nil)
                    
//                   self.dismiss(animated: true, completion: nil)
                }
//                self.dismiss(animated: true, completion: nil)
                
            }
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "receipt"{
            let Object = segue.destination as! receiptViewController
            Object.orders = orders
            Object.Index = Index
            Object.itemList = newItemList
        }
    }
    
    func getHeadingForDirection(fromCoordinate fromLoc: CLLocationCoordinate2D, toCoordinate toLoc: CLLocationCoordinate2D) -> Float {
        
        let fLat: Float = Float((fromLoc.latitude).degreesToRadians)
        let fLng: Float = Float((fromLoc.longitude).degreesToRadians)
        let tLat: Float = Float((toLoc.latitude).degreesToRadians)
        let tLng: Float = Float((toLoc.longitude).degreesToRadians)
        let degree: Float = (atan2(sin(tLng - fLng) * cos(tLat), cos(fLat) * sin(tLat) - sin(fLat) * cos(tLat) * cos(tLng - fLng))).radiansToDegrees
        if degree >= 0 {
            return degree
        }
        else {
            return 360 + degree
        }
    }
}


extension HomeViewController:UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
   
        if flag == "status"{
            return newItemList.count
        }
        else{
            return itemList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as! ItemListTableViewCell
        
        cell.itemView.layer.cornerRadius = 10
        cell.itemView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        cell.itemView.layer.borderWidth = 1
        
        cell.itemImage.layer.cornerRadius = 10
        cell.itemImage.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        cell.itemImage.layer.borderWidth = 1
        
        if flag == "status"{
            cell.itemName.text = newItemList[indexPath.row].name
            cell.itemDesc.text = newItemList[indexPath.row].description
            cell.height.text = newItemList[indexPath.row].height
            cell.width.text = newItemList[indexPath.row].width
            cell.weight.text = newItemList[indexPath.row].weight
            
            
            cell.itemImage.sd_setImage(with: URL(string: newItemList[indexPath.row].item_image), placeholderImage: UIImage(named: "item_placeholder"))
//            cell.itemImage.image = itemList[indexPath.row].item_image
        }
        else{
            cell.itemName.text = itemList[indexPath.row].name
            cell.itemDesc.text = itemList[indexPath.row].description
            cell.height.text = itemList[indexPath.row].height
            cell.width.text = itemList[indexPath.row].width
            cell.weight.text = itemList[indexPath.row].weight
            
            cell.itemImage.image = itemList[indexPath.row].item_image
        }
      
        
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 124
        
    }
    

}
extension Int {
    var degreesToRadians: Double { return Double(self) * .pi / 180 }
}
extension FloatingPoint {
    var degreesToRadians: Self { return self * .pi / 180 }
    var radiansToDegrees: Self { return self * 180 / .pi }
}



extension UIImageView {
   func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
      URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
   }
   func downloadImage(from url: URL) {
      getData(from: url) {
         data, response, error in
         guard let data = data, error == nil else {
            return
         }
         DispatchQueue.main.async() {
            self.image = UIImage(data: data)
         }
      }
   }
}
