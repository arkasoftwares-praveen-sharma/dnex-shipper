//
//  OtherInfoViewController.swift
//  Dnex Shipper
//
//  Created by Arka on 26/02/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import BSImagePicker
import Photos
import SwiftyJSON


class OtherInfoViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var customCollection: UICollectionView!
    @IBOutlet weak var DocImageView: UIImageView!
    @IBOutlet weak var submitBtnoutlet: UIButton!
    @IBOutlet weak var vehicleNumber: ACFloatingTextfield!
    @IBOutlet weak var capacity: ACFloatingTextfield!
    
    
    var bank_Status = ""
    
    var AssetsArray = NSMutableArray()
    var retimage = UIImage()
    var number = 5
    var ImageType = ""
    
    var imageDataArr:[Data] = []
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        DocImageView.layer.cornerRadius = 10
        DocImageView.clipsToBounds = true
          GIFHUD.shared.setGif(named: "loader.gif")
        submitBtnoutlet.layer.cornerRadius = submitBtnoutlet.frame.height/2
        submitBtnoutlet.clipsToBounds = true
        
        capacity.delegate = self
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == capacity{
            let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
            return string.rangeOfCharacter(from: invalidCharacters) == nil
        }
        return true

    }
    
    //MARK:- show custom image picker
    func showCustomImagePicker() {
        let vc = BSImagePickerViewController()
        print(number)
        vc.maxNumberOfSelections = number
        vc.takePhotoIcon = UIImage(named: "chat")
        
        vc.albumButton.tintColor = UIColor.green
        vc.cancelButton.tintColor = UIColor.red
        vc.doneButton.tintColor = UIColor.purple
        vc.selectionCharacter = "✓"
        vc.selectionFillColor = UIColor.gray
        vc.selectionStrokeColor = UIColor.yellow
        vc.selectionShadowColor = UIColor.red
        vc.selectionTextAttributes[NSAttributedString.Key.foregroundColor] = UIColor.lightGray
        vc.cellsPerRow = {(verticalSize: UIUserInterfaceSizeClass, horizontalSize: UIUserInterfaceSizeClass) -> Int in
            switch (verticalSize, horizontalSize) {
            case (.compact, .regular): // iPhone5-6 portrait
                return 2
            case (.compact, .compact): // iPhone5-6 landscape
                return 2
            case (.regular, .regular): // iPad portrait/landscape
                return 3
            default:
                return 2
            }
        }

        bs_presentImagePickerController(vc, animated: true,
                                        select: { (asset: PHAsset) -> Void in
                                            
//                                            self.AssetsArray.add(asset)
                                            
        }, deselect: { (asset: PHAsset) -> Void in
//            self.AssetsArray.remove(asset)
            print("Deselected: \(asset)")
        }, cancel: { (assets: [PHAsset]) -> Void in
            print("Cancel: \(assets)")
        }, finish: { (assets: [PHAsset]) -> Void in

            for item in assets{
                self.AssetsArray.add(item)
            }
            self.customCollection.reloadData()
            print("Finish: \(assets)")
        }, completion: nil)
    }
    
    func getAssetThumbnail(asset: PHAsset) {
        let manager = PHImageManager.default()
        manager.requestImage(for: asset, targetSize: CGSize(width: 100.0, height: 100.0), contentMode: .aspectFit, options: nil, resultHandler: {
            (result, info)->Void in
            self.retimage = result!
            // callReturnImage(retimage) // <- create this method
        })
    }
    
    @IBAction func selectImageBtnClicked(_ sender: Any) {
        
        if AssetsArray.count == 5{
            CommonFunctions.showErrorAlert(uiRef: self, message: "You can add maximum 5 images !!".localized)
        }
        else{
            
            let alert = UIAlertController(title: "Choose Image".localized, message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Camera".localized, style: .default, handler: { _ in
                self.ImageType = "vehicle"
                self.openCamera()
            }))
            
            alert.addAction(UIAlertAction(title: "Gallery".localized, style: .default, handler: { _ in
                self.number = 5 - self.AssetsArray.count
                self.ImageType = "vehicle"
                self.showCustomImagePicker()
            }))
            
            alert.addAction(UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    @IBAction func UploadDocumentBtnClicked(_ sender: Any) {
        
        let alert = UIAlertController(title: "Choose Image".localized, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera".localized, style: .default, handler: { _ in
            self.ImageType = "document"
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery".localized, style: .default, handler: { _ in
            self.ImageType = "document"
            self.openGallery()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel".localized, style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func openCamera()
    {
        //        ANLoader.showLoading("Please wait", disableUI: false)
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallery()
    {
        //        ANLoader.showLoading("Please wait", disableUI: false)
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning".localized, message: "You don't have permission to access gallery.".localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        
        
        if vehicleNumber.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter vehicle's number !!".localized)
        }
            
        else if capacity.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter vehicle's capacity !!".localized)
        }
        else if AssetsArray.count != 5{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Add 5 images of the vehicle !!".localized)
        }
        else if DocImageView.image == nil{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Upload your driving license !!".localized)
        }
            
        else{
            
             GIFHUD.shared.show(withOverlay: true, duration: 2)
            
            for index in 0...self.AssetsArray.count - 1 {
                
                
                let asset = self.AssetsArray.object(at: index) as! PHAsset
                let options = PHImageRequestOptions()
                var oriznalImg = UIImage()
                
                options.isSynchronous = true
                options.deliveryMode = .highQualityFormat
                options.resizeMode = .exact
                
                if (asset.mediaType == PHAssetMediaType.image) {
                    
                    let requestOptions = PHImageRequestOptions()
                    requestOptions.isSynchronous = true // adjust the parameters as you wish
                    PHImageManager.default().requestImageData(for: asset, options: requestOptions, resultHandler: { (imageData, UTI, _, _) in
                        let dd = Data(bytes: imageData!)
                        
                        let imagePP = UIImage(data: dd as Data)
                        let imageData1 = imagePP!.jpegData(compressionQuality: 0.5)!
                        
                        
                        print(imageData1)
                        
                        self.imageDataArr.append(imageData1)
                        
                    })
                }
            }
            
            NetworkManager.vehicleDetail(webservice: "add-shipper-vehicle-detail", vehicle_no: vehicleNumber.text!, capacity: capacity.text!, vehicle_images: imageDataArr, national_id: DocImageView.image!){sJson in
                GIFHUD.shared.dismiss()
                let sJSON = JSON(sJson)
                
                print(sJSON)
                
                if sJSON["status"].intValue == 200{
                    if self.bank_Status == "0"{
                        self.performSegue(withIdentifier: "bank", sender: nil)
                    }
                    else{
                        self.performSegue(withIdentifier: "login", sender: nil)
                    }
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
}

extension OtherInfoViewController:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return AssetsArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath) as! OtherInfoCollectionViewCell
        
        getAssetThumbnail(asset: AssetsArray[indexPath.row] as! PHAsset)
                   
        cell.customImage.image = retimage
        
        cell.customImage.layer.cornerRadius = 10
        cell.customImage.clipsToBounds = true
        
        cell.customImage.layer.borderWidth = 1
        cell.customImage.layer.borderColor = UIColor.lightGray.cgColor
        
        cell.deletBtn.tag = indexPath.row
        cell.deletBtn.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
        
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:100, height:100);
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        
        AssetsArray.removeObject(at: sender.tag)
        number = 5
        
        print(number)
        customCollection.reloadData()
        
    }
    
}

extension OtherInfoViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        dismiss(animated: true, completion: nil)
        //        ANLoader.showLoading("Please wait", disableUI: false)
        print(info)
        
        //        let pickedImg = info[.originalImage] as? UIImage
        //
        //        ProfileImg.image = pickedImg
        
        
        if ImageType == "vehicle"{
//            if let PHASSET = info[UIImagePickerController.InfoKey.phAsset] as? PHAsset{
//                AssetsArray.add(PHASSET)
//                customCollection.reloadData()
//            }
            
            if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                
//                DocImageView.image = pickedImage
                
                
                UIImageWriteToSavedPhotosAlbum(pickedImage, nil, nil, nil)
                
//                 UIImageWriteToSavedPhotosAlbum(pickedImage, self, #selector(cameraImageSavedAsynchronously), nil)
             
                
                let fetchOptions = PHFetchOptions()
                   fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                   fetchOptions.fetchLimit = 1

                let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                   if (fetchResult.firstObject != nil)
                   {
                    let lastImageAsset: PHAsset = fetchResult.firstObject!
                    print(lastImageAsset)
                    
                    AssetsArray.add(lastImageAsset)
                   
                    customCollection.reloadData()
                   }
                
                
            }
           

        }
        else{
      
            if let pickedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                
                DocImageView.image = pickedImage
                
            }
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
//        ANLoader.hide()
    }
    
  
}
