//
//  orderDetailViewController.swift
//  Dnex
//
//  Created by Arka on 04/03/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit


class orderDetailViewController: UIViewController {

     var itemList:[item_order] = []
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var puckUpView: UIView!
    @IBOutlet weak var receiverInitials: UILabel!
    
    @IBOutlet weak var shipperRateBtnOutlet: UIButton!
    @IBOutlet weak var pickViewRound: UIView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var stateImg: UIImageView!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var trackBtnOutlet: UIButton!
    @IBOutlet weak var backBtnOutlet: UIButton!
    @IBOutlet weak var rewardHeight: NSLayoutConstraint!
    
    
    
    
    
    @IBOutlet weak var orderIdLbl: UILabel!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var pickUp: UILabel!
    @IBOutlet weak var receiverName: UILabel!
    
    @IBOutlet weak var receiverMobile: UILabel!
    
    @IBOutlet weak var receiverAddress: UILabel!
    @IBOutlet weak var shipper_Img: UIImageView!
    
    @IBOutlet weak var shipperName: UILabel!
    
    @IBOutlet weak var shipper_Vehicle: UILabel!
    
    @IBOutlet weak var shipper_rating: UIButton!
    
    @IBOutlet weak var delivery_type: UILabel!
    
    @IBOutlet weak var payment_type: UILabel!
    
    @IBOutlet weak var subTotal: UILabel!
    
    @IBOutlet weak var discountLbl: UILabel!
    
    @IBOutlet weak var totalLbl: UILabel!
    
    
    
    var StateStr = ""
    var orderStatus = ""
    
    
    var Index = 0
    var orders:[current] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        itemList.append(item.init(name: "chair", description: "Test description for chair", height: "20", weight: "10", width: "2", item_image: UIImage (named: "noPathCopy2")!))
//        itemList.append(item.init(name: "Table", description: "Test description for table", height: "20", weight: "10", width: "2", item_image: UIImage (named: "noPathCopy2")!))
//        itemList.append(item.init(name: "chair", description: "Test description for chair", height: "20", weight: "10", width: "2", item_image: UIImage (named: "noPathCopy2")!))
        

//        let constantHeight:CGFloat = CGFloat((124 * itemList.count) + 300)

        heightConstraint.constant = CGFloat((124 * itemList.count) + 1001)
        headView.setShadow()
        
        
        pickViewRound.layer.cornerRadius = pickViewRound.frame.height/2
        receiverInitials.layer.cornerRadius = receiverInitials.frame.height/2
        receiverInitials.clipsToBounds = true
        shipperRateBtnOutlet.layer.cornerRadius = shipperRateBtnOutlet.frame.height/2
        
        
        puckUpView.layer.cornerRadius = puckUpView.frame.height/2
        puckUpView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        puckUpView.layer.borderWidth = 1
        
        
        if StateStr == "current"{
            
            headerView.backgroundColor = UIColor.white
            headerTitle.textColor = UIColor.black
            trackBtnOutlet.isHidden = false
            
            trackBtnOutlet.layer.cornerRadius = trackBtnOutlet.frame.height/2
            headerView.setShadow()
            stateImg.isHidden = true
            stateLbl.isHidden = true
            backBtnOutlet.setImage(UIImage (named: "backBlue"), for: .normal)
            
        }
        else if StateStr == "past"{
            
           
            headerTitle.textColor = UIColor.white
            trackBtnOutlet.isHidden = true
            
            stateImg.isHidden = false
            stateLbl.isHidden = false
            
            if orderStatus == "completed"{
                headerView.backgroundColor = UIColor().HexToColor(hexString: "18d293")
                stateImg.image = UIImage (named: "completed")
                stateLbl.textColor = UIColor().HexToColor(hexString: "18d293")
                stateLbl.text = "Completed".localized
            }
            else{
                headerView.backgroundColor = UIColor().HexToColor(hexString: "db2f2f")
                stateImg.image = UIImage (named: "cancel")
                stateLbl.textColor = UIColor().HexToColor(hexString: "db2f2f")
                stateLbl.text = "Cancelled".localized
                
//                rewardHeight.constant = 0
                heightConstraint.constant = heightConstraint.constant - 120
                
            }
            
            backBtnOutlet.setImage(UIImage (named: "whiteBack"), for: .normal)
        }
        else{
            headerView.backgroundColor = UIColor().HexToColor(hexString: "f9c63a")
            headerTitle.textColor = UIColor.white
            trackBtnOutlet.isHidden = true
            
            stateImg.isHidden = false
            stateLbl.isHidden = false
            
            stateImg.image = UIImage (named: "workIcon")
            stateLbl.text = "Pending".localized
            stateLbl.textColor = UIColor().HexToColor(hexString: "f9c63a")
            backBtnOutlet.setImage(UIImage (named: "whiteBack"), for: .normal)
        }
        
        
        shipper_Img.layer.cornerRadius = shipper_Img.frame.height/2
        
        /// Add data to UI
        
        orderIdLbl.text = "ORDER ID:".localized + " \(orders[Index].order_id)"
        dateTime.text = orders[Index].date
        pickUp.text = orders[Index].pickUp
        receiverName.text = orders[Index].receiver_name
        receiverMobile.text = orders[Index].receiver_mobile_code + " " + orders[Index].receiver_mobile
        receiverAddress.text = orders[Index].receiver_address
        shipperName.text = orders[Index].sender_name
        shipper_Vehicle.text = orders[Index].shipper_vehicle_number
        delivery_type.text = orders[Index].delivery_type
        payment_type.text = orders[Index].payment_method
        subTotal.text = orders[Index].sub_total
        discountLbl.text = orders[Index].discount
        totalLbl.text = orders[Index].total
        
        shipperRateBtnOutlet.setTitle("  " + orders[Index].shipper_rating, for: .normal)
        
        let image = orders[Index].sender_image
        
        self.shipper_Img.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "defaultProfile"))
        
        let initials = receiverName.text!.components(separatedBy: " ").reduce("") { ($0 == "" ? "" : "\($0.first!)") + "\($1.first!)" }
        
        receiverInitials.text = initials
        
    }

    
    @IBAction func backBtnClicked(_ sender: Any) {
        
//        self.dismiss(animated: true, completion: nil)
        
        let VC = self.presentingViewController
        
        self.dismiss(animated: true) {
            VC?.viewDidLoad()
        }
        
    }
}

extension orderDetailViewController:UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as! ItemListTableViewCell
        
        cell.itemView.layer.cornerRadius = 10
        cell.itemView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        cell.itemView.layer.borderWidth = 1
        
        cell.itemImage.layer.cornerRadius = 10
        cell.itemImage.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        cell.itemImage.layer.borderWidth = 1
        
        cell.itemName.text = itemList[indexPath.row].name
        cell.itemDesc.text = itemList[indexPath.row].description
        cell.height.text = itemList[indexPath.row].height
        cell.width.text = itemList[indexPath.row].width
        cell.weight.text = itemList[indexPath.row].weight
        
//        cell.itemImage.image = itemList[indexPath.row].item_image
        cell.itemImage.sd_setImage(with: URL(string: itemList[indexPath.row].item_image), placeholderImage: UIImage(named: "item_placeholder"))
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 124
        
    }
    
}
