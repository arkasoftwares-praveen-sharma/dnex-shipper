//
//  myOrderViewController.swift
//  Dnex
//
//  Created by Arka on 04/03/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import SwiftyJSON

struct current {
    var type = ""
    var id = ""
    var order_id = ""
    var date = ""
    var pickUp = ""
    var receiver_name = ""
    var receiver_mobile_code = ""
    var receiver_mobile = ""
    var receiver_address = ""
    var delivery_type = ""
    var payment_method = ""
    var sub_total = ""
    var discount = ""
    var total = ""
    var shipper_image = ""
    var shipper_name = ""
    var shipper_rating = ""
    var shipper_vehicle_number = ""
    var sender_image = ""
    var sender_name = ""
    var sender_lat = ""
    var sender_long = ""
    var receiver_lat = ""
    var receiver_long = ""
    var sender_mobile_code = ""
    var sender_mobile = ""
    var sender_id = ""
    var order_type = ""
}

struct item_order {
    var name = ""
    var description = ""
    var height = ""
    var weight = ""
    var width = ""
    var item_image = ""
}


class myOrderViewController: UIViewController {

    @IBOutlet weak var pastImg: UIImageView!

    @IBOutlet weak var scheduleImg: UIImageView!
    
    @IBOutlet weak var customTable: UITableView!
    @IBOutlet weak var customView: UIView!
    @IBOutlet weak var addBtnOutlet: UIButton!
    
    @IBOutlet weak var send: UIButton!
    @IBOutlet weak var tooFar: UIButton!
    @IBOutlet weak var mistake: UIButton!
    @IBOutlet weak var other: UIButton!
    
    @IBOutlet weak var messageTxt: ACFloatingTextfield!
    
    @IBOutlet weak var cancelHeight: NSLayoutConstraint!
    @IBOutlet weak var sideBtn: UIButton!
    
    var groups = [[item_order]]()
    var groups_past = [[item_order]]()
    
    
    var current_orders:[current] = []
    var past_orders:[current] = []
    var orderArr:[String] = []
    var past_orderArr:[String] = []
    var orderStatus = ""
    
    var current_Index = 0
    var itemList:[item_order] = []
    var itemList_send:[item_order] = []

    var tabString = "past"
    
    
     @objc func setToPeru(){
        
        
        self.past_orders.removeAll()
        self.current_orders.removeAll()
       
        GIFHUD.shared.show(withOverlay: true, duration: 2)
        NetworkManager.myOrders(uiRef: self, Webservice: "order-history"){sJson in
            let sJSON = JSON(sJson)
            print(sJSON)
            if sJSON["status"].intValue == 200{
                
                
                CommonFunctions.delay(1.0) {
                    GIFHUD.shared.dismiss()
                    ///          Current Orders
                    self.groups.removeAll()
                    self.groups_past.removeAll()
                    
                    for item in sJSON["data"]["current_order"].arrayValue{
                        
                        let type = item["type"].stringValue
                        let id = item["id"].stringValue
                        let order_id = item["order_id"].stringValue
                        let receiver_name = item["receiver_name"].stringValue
                        let pickup_address = item["pickup_address"].stringValue
                        let receiver_country_code = item["receiver_country_code"].stringValue
                        let receiver_mobile = item["receiver_mobile"].stringValue
                        let receiver_address = item["receiver_address"].stringValue
                        let delivery_type = item["delivery_type"].stringValue
                        let payment_type = item["get_payment_detail"]["payment_type"].stringValue
                        let total = item["get_payment_detail"]["amount"].stringValue
                        let created_at = item["date"].stringValue
                        let discount_per = item["get_applied_coupon"]["coupon_data"]["discount_percentage"].stringValue
                        let shipper_image = "http://103.207.168.164:8008/storage/\(item["get_assign_shipper"]["get_shipper_details"]["profile_pic"].stringValue)"
                        let shipper_name = item["get_assign_shipper"]["get_shipper_details"]["name"].stringValue
                        let shipper_vehicle_number = item["get_assign_shipper"]["get_shipper_details"]["get_vehicle_details"]["vehicle_no"].stringValue
                        
                        let sender_name = item["get_user_details"]["name"].stringValue
                        let sender_image = "http://103.207.168.164:8008/storage/\(item["get_user_details"]["profile_pic"].stringValue)"
                        let customer_rating = item["customer_rating"].stringValue
                        
                        let sender_lat = item["pickup_lat"].stringValue
                        let sender_long = item["pickup_lng"].stringValue
                        
                        let receiver_lat = item["receiver_lat"].stringValue
                        let receiver_long = item["receiver_lng"].stringValue
                        
                        let sender_number_code = item["get_user_details"]["country_code"].stringValue
                        let sender_number = item["get_user_details"]["mobile"].stringValue
                        let sender_id = item["get_user_details"]["id"].stringValue
                        var totalString = ""
                        var discountStr = ""
                        if discount_per != ""{
                            let dis_per = Double(discount_per) ?? 0.0
                            let sub_totalFloat = Double(total) ?? 0.0
                            let discount_amt:Double = round(dis_per * (sub_totalFloat/100))
                            //                        discount_amt = Double(discount_amt.cleanValue)!
                            totalString = String(format:"%.2f",sub_totalFloat  + discount_amt)
                            totalString = totalString.replacingOccurrences(of: ".00", with: "")
                            discountStr = String(format:"%.2f",discount_amt)
                            discountStr = discountStr.replacingOccurrences(of: ".00", with: "")
                            
                        }
                        else{
                            discountStr = "0"
                            totalString = total
                        }
                        
                        let status = item["get_order_status"].arrayValue
                        
                        
                        let NewItem = status.last
                        
                        let newStatus = NewItem?["status"].stringValue
                        if newStatus == "3"{
                            self.orderArr.append("In Transit")
                        }
                        else if newStatus == "1"{
                            self.orderArr.append("Pending")
                        }
                        else {
                            self.orderArr.append("Picked Up")
                        }
                        
                        
                        var new_item_arr:[item_order] = []
                        for item_detail in item["get_product_details"].arrayValue{
                            
                            let image = "http://103.207.168.164:8008/storage/\(item_detail["image"].stringValue)"
                            let name = item_detail["name"].stringValue
                            let height = "\(item_detail["height"].stringValue) cm"
                            let width = "\(item_detail["width"].stringValue) cm"
                            let weight = "\(item_detail["weight"].stringValue) kg"
                            let description_item = item_detail["description"].stringValue
                            
                            
                            new_item_arr.append(item_order.init(name: name, description: description_item, height: height, weight: weight, width: width, item_image:image ))
                            
                        }
                        
                        self.groups.append(new_item_arr)
                        
                        self.current_orders.append(current.init(type: type, id: id, order_id: order_id, date: created_at, pickUp: pickup_address, receiver_name: receiver_name, receiver_mobile_code: receiver_country_code, receiver_mobile: receiver_mobile, receiver_address: receiver_address, delivery_type: delivery_type, payment_method: payment_type, sub_total: "$" + " " + totalString, discount: "- AED " + discountStr, total: "$" + " " + total, shipper_image: shipper_image, shipper_name: shipper_name, shipper_rating: customer_rating, shipper_vehicle_number: shipper_vehicle_number,sender_image:sender_image,sender_name: sender_name,sender_lat: sender_lat,sender_long: sender_long,receiver_lat: receiver_lat,receiver_long: receiver_long,sender_mobile_code: sender_number_code,sender_mobile: sender_number,sender_id: sender_id ))
                        
                    }
                    
                    //                    if self.current_orders.count == 0{
                    //                        self.customTable.isHidden = true
                    //                    }
                    //                    else{
                    //                        self.customTable.isHidden = false
                    //                    }
                    
                    
                    ///          Past Orders
                    
                    for item in sJSON["data"]["past_order"].arrayValue{
                        
                        let type = item["type"].stringValue
                        let id = item["id"].stringValue
                        let order_id = item["order_id"].stringValue
                        let receiver_name = item["receiver_name"].stringValue
                        let pickup_address = item["pickup_address"].stringValue
                        let receiver_country_code = item["receiver_country_code"].stringValue
                        let receiver_mobile = item["receiver_mobile"].stringValue
                        let receiver_address = item["receiver_address"].stringValue
                        let delivery_type = item["delivery_type"].stringValue
                        let payment_type = item["get_payment_detail"]["payment_type"].stringValue
                        let total = item["get_payment_detail"]["amount"].stringValue
                        let created_at = item["date"].stringValue
                        let discount_per = item["get_applied_coupon"]["coupon_data"]["discount_percentage"].stringValue
                        let shipper_image = "http://103.207.168.164:8008/storage/\(item["get_assign_shipper"]["get_shipper_details"]["profile_pic"].stringValue)"
                        let shipper_name = item["get_assign_shipper"]["get_shipper_details"]["name"].stringValue
                        let shipper_vehicle_number = item["get_assign_shipper"]["get_shipper_details"]["get_vehicle_details"]["vehicle_no"].stringValue
                        let status = item["get_order_status"].arrayValue
                        let sender_name = item["get_user_details"]["name"].stringValue
                        let sender_image = "http://103.207.168.164:8008/storage/\(item["get_user_details"]["profile_pic"].stringValue)"
                        let customer_rating = item["customer_rating"].stringValue
                        let NewItem = status.last
                        
                        let newStatus = NewItem?["status"].stringValue
                        if newStatus == "5"{
                            self.past_orderArr.append("completed")
                        }
                        else{
                            self.past_orderArr.append("cancelled")
                        }
                        
                        var totalString = ""
                        var discountStr = ""
                        if discount_per != ""{
                            let dis_per = Double(discount_per) ?? 0.0
                            let sub_totalFloat = Double(total) ?? 0.0
                            let discount_amt:Double = round(dis_per * (sub_totalFloat/100))
                            //                        discount_amt = Double(discount_amt.cleanValue)!
                            totalString = String(format:"%.2f",sub_totalFloat  + discount_amt)
                            totalString = totalString.replacingOccurrences(of: ".00", with: "")
                            discountStr = String(format:"%.2f",discount_amt)
                            discountStr = discountStr.replacingOccurrences(of: ".00", with: "")
                            
                        }
                        else{
                            discountStr = "0"
                            totalString = total
                        }
                        
                        var new_item_arr:[item_order] = []
                        
                        
                        for item_detail in item["get_product_details"].arrayValue{
                            
                            let image = "http://103.207.168.164:8008/storage/\(item_detail["image"].stringValue)"
                            let name = item_detail["name"].stringValue
                            let height = "\(item_detail["height"].stringValue) cm"
                            let width = "\(item_detail["width"].stringValue) cm"
                            let weight = "\(item_detail["weight"].stringValue) kg"
                            let description_item = item_detail["description"].stringValue
                            
                            new_item_arr.append(item_order.init(name: name, description: description_item, height: height, weight: weight, width: width, item_image:image ))
                            
                        }
                        
                        self.groups_past.append(new_item_arr)
                        
                        
                        self.past_orders.append(current.init(type: type, id: id, order_id: order_id, date: created_at, pickUp: pickup_address, receiver_name: receiver_name, receiver_mobile_code: receiver_country_code, receiver_mobile: receiver_mobile, receiver_address: receiver_address, delivery_type: delivery_type, payment_method: payment_type, sub_total: "$" + " " + totalString, discount: "- AED " + discountStr, total: "$" + " " + total, shipper_image: shipper_image, shipper_name: shipper_name, shipper_rating: customer_rating, shipper_vehicle_number: shipper_vehicle_number,sender_image:sender_image,sender_name: sender_name,sender_lat: "",sender_long: "",receiver_lat: "",receiver_long: "",sender_mobile_code: "",sender_mobile: ""))
                        
                    }
                    
                    self.customTable.reloadData()
                }
                
            }
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
            self.tabString = "current"
            self.pastImg.image = UIImage (named: "ellipse25")
            self.scheduleImg.image = nil
            self.customTable.reloadData()
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
         NotificationCenter.default.addObserver(self, selector: #selector(setToPeru), name: .cart, object: nil)
        
        addBtnOutlet.layer.cornerRadius = addBtnOutlet.frame.height/2
        pastImg.image = UIImage (named: "ellipse25")
        cancelHeight.constant = 420
        
          GIFHUD.shared.setGif(named: "loader.gif")
        
        if self.revealViewController() != nil {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            sideBtn.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
            self.revealViewController().rearViewRevealWidth = self.view.frame.width
        }
        

        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.past_orders.removeAll()
        self.current_orders.removeAll()
        
        GIFHUD.shared.show(withOverlay: true, duration: 2)
        NetworkManager.myOrders(uiRef: self, Webservice: "order-history"){sJson in
            let sJSON = JSON(sJson)
            print(sJSON)
            if sJSON["status"].intValue == 200{
                
                
                CommonFunctions.delay(1.0) {
                    GIFHUD.shared.dismiss()
                    ///          Current Orders
                    self.groups.removeAll()
                    self.groups_past.removeAll()
                    
                    for item in sJSON["data"]["current_order"].arrayValue{
                        
                        let type = item["type"].stringValue
                        let id = item["id"].stringValue
                        let order_id = item["order_id"].stringValue
                        let receiver_name = item["receiver_name"].stringValue
                        let pickup_address = item["pickup_address"].stringValue
                        let receiver_country_code = item["receiver_country_code"].stringValue
                        let receiver_mobile = item["receiver_mobile"].stringValue
                        var receiver_address = item["receiver_address"].stringValue
                        if receiver_address == ""{
                            receiver_address = "Receiver will schedule"
                        }
                        let delivery_type = item["delivery_type"].stringValue
                        let payment_type = item["get_payment_detail"]["payment_type"].stringValue
                        let order_type = "AED \(item["get_payment_detail"]["sell_amount"].stringValue)"
                        let total = item["get_payment_detail"]["amount"].stringValue
                        let created_at = item["date"].stringValue
                        let discount_per = item["get_applied_coupon"]["coupon_data"]["discount_percentage"].stringValue
                        let shipper_image = "http://103.207.168.164:8008/storage/\(item["get_assign_shipper"]["get_shipper_details"]["profile_pic"].stringValue)"
                        let shipper_name = item["get_assign_shipper"]["get_shipper_details"]["name"].stringValue
                        let shipper_vehicle_number = item["get_assign_shipper"]["get_shipper_details"]["get_vehicle_details"]["vehicle_no"].stringValue
                        
                        let sender_name = item["get_user_details"]["name"].stringValue
                        let sender_image = "http://103.207.168.164:8008/storage/\(item["get_user_details"]["profile_pic"].stringValue)"
                        let customer_rating = item["customer_rating"].stringValue
                        let sender_lat = item["pickup_lat"].stringValue
                        let sender_long = item["pickup_lng"].stringValue
                        
                        let receiver_lat = item["receiver_lat"].stringValue
                        let receiver_long = item["receiver_lng"].stringValue
                        
                        let sender_number_code = item["get_user_details"]["country_code"].stringValue
                        let sender_number = item["get_user_details"]["mobile"].stringValue
                        let sender_id = item["get_user_details"]["id"].stringValue
                        var totalString = ""
                        var discountStr = ""
                        if discount_per != ""{
                            let dis_per = Double(discount_per) ?? 0.0
                            let sub_totalFloat = Double(total) ?? 0.0
                            let discount_amt:Double = round(dis_per * (sub_totalFloat/100))
                            //                        discount_amt = Double(discount_amt.cleanValue)!
                            totalString = String(format:"%.2f",sub_totalFloat  + discount_amt)
                            totalString = totalString.replacingOccurrences(of: ".00", with: "")
                            discountStr = String(format:"%.2f",discount_amt)
                            discountStr = discountStr.replacingOccurrences(of: ".00", with: "")
                            
                        }
                        else{
                            discountStr = "0"
                            totalString = total
                        }
                        
                        let status = item["get_order_status"].arrayValue
                        
                        
                        let NewItem = status.last
                        
                        let newStatus = NewItem?["status"].stringValue
                        if newStatus == "3"{
                            self.orderArr.append("In Transit")
                        }
                        else if newStatus == "1"{
                            self.orderArr.append("Pending")
                        }
                        else {
                            self.orderArr.append("Picked Up")
                        }
                        
                        
                        var new_item_arr:[item_order] = []
                        for item_detail in item["get_product_details"].arrayValue{
                            
                            let image = "http://103.207.168.164:8008/storage/\(item_detail["image"].stringValue)"
                            let name = item_detail["name"].stringValue
                            let height = "\(item_detail["height"].stringValue) cm"
                            let width = "\(item_detail["width"].stringValue) cm"
                            let weight = "\(item_detail["weight"].stringValue) kg"
                            let description_item = item_detail["description"].stringValue
                            
                            
                            new_item_arr.append(item_order.init(name: name, description: description_item, height: height, weight: weight, width: width, item_image:image ))
                            
                        }
                        
                        self.groups.append(new_item_arr)
                        
                        self.current_orders.append(current.init(type: type, id: id, order_id: order_id, date: created_at, pickUp: pickup_address, receiver_name: receiver_name, receiver_mobile_code: receiver_country_code, receiver_mobile: receiver_mobile, receiver_address: receiver_address, delivery_type: delivery_type, payment_method: payment_type, sub_total: "$" + " " + totalString, discount: "- AED " + discountStr, total: "$" + " " + total, shipper_image: shipper_image, shipper_name: shipper_name, shipper_rating: customer_rating, shipper_vehicle_number: shipper_vehicle_number,sender_image:sender_image,sender_name: sender_name,sender_lat: sender_lat,sender_long: sender_long,receiver_lat: receiver_lat,receiver_long: receiver_long,sender_mobile_code: sender_number_code,sender_mobile: sender_number,sender_id: sender_id,order_type: order_type))
                        
                    }
                    
                    //                    if self.current_orders.count == 0{
                    //                        self.customTable.isHidden = true
                    //                    }
                    //                    else{
                    //                        self.customTable.isHidden = false
                    //                    }
                    
                    
                    ///          Past Orders
                    
                    for item in sJSON["data"]["past_order"].arrayValue{
                        
                        let type = item["type"].stringValue
                        let id = item["id"].stringValue
                        let order_id = item["order_id"].stringValue
                        let receiver_name = item["receiver_name"].stringValue
                        let pickup_address = item["pickup_address"].stringValue
                        let receiver_country_code = item["receiver_country_code"].stringValue
                        let receiver_mobile = item["receiver_mobile"].stringValue
                        var receiver_address = item["receiver_address"].stringValue
                        if receiver_address == ""{
                            receiver_address = "Receiver will schedule"
                        }
                        let delivery_type = item["delivery_type"].stringValue
                        let payment_type = item["get_payment_detail"]["payment_type"].stringValue
                        let total = item["get_payment_detail"]["amount"].stringValue
                        let created_at = item["date"].stringValue
                        let discount_per = item["get_applied_coupon"]["coupon_data"]["discount_percentage"].stringValue
                        let shipper_image = "http://103.207.168.164:8008/storage/\(item["get_assign_shipper"]["get_shipper_details"]["profile_pic"].stringValue)"
                        let shipper_name = item["get_assign_shipper"]["get_shipper_details"]["name"].stringValue
                        let shipper_vehicle_number = item["get_assign_shipper"]["get_shipper_details"]["get_vehicle_details"]["vehicle_no"].stringValue
                        let status = item["get_order_status"].arrayValue
                        
                        let sender_name = item["get_user_details"]["name"].stringValue
                        let sender_image = "http://103.207.168.164:8008/storage/\(item["get_user_details"]["profile_pic"].stringValue)"
                        let customer_rating = item["customer_rating"].stringValue
                        
                        let NewItem = status.last
                        
                        let newStatus = NewItem?["status"].stringValue
                        if newStatus == "5"{
                            self.past_orderArr.append("completed")
                        }
                        else{
                            self.past_orderArr.append("cancelled")
                        }
                        
                        var totalString = ""
                        var discountStr = ""
                        if discount_per != ""{
                            let dis_per = Double(discount_per) ?? 0.0
                            let sub_totalFloat = Double(total) ?? 0.0
                            let discount_amt:Double = round(dis_per * (sub_totalFloat/100))
                            //                        discount_amt = Double(discount_amt.cleanValue)!
                            totalString = String(format:"%.2f",sub_totalFloat  + discount_amt)
                            totalString = totalString.replacingOccurrences(of: ".00", with: "")
                            discountStr = String(format:"%.2f",discount_amt)
                            discountStr = discountStr.replacingOccurrences(of: ".00", with: "")
                            
                        }
                        else{
                            discountStr = "0"
                            totalString = total
                        }
                        
                        var new_item_arr:[item_order] = []
                        
                        
                        for item_detail in item["get_product_details"].arrayValue{
                            
                            let image = "http://103.207.168.164:8008/storage/\(item_detail["image"].stringValue)"
                            let name = item_detail["name"].stringValue
                            let height = "\(item_detail["height"].stringValue) cm"
                            let width = "\(item_detail["width"].stringValue) cm"
                            let weight = "\(item_detail["weight"].stringValue) kg"
                            let description_item = item_detail["description"].stringValue
                            
                            new_item_arr.append(item_order.init(name: name, description: description_item, height: height, weight: weight, width: width, item_image:image ))
                            
                        }
                        
                        self.groups_past.append(new_item_arr)
                        
                        
                        self.past_orders.append(current.init(type: type, id: id, order_id: order_id, date: created_at, pickUp: pickup_address, receiver_name: receiver_name, receiver_mobile_code: receiver_country_code, receiver_mobile: receiver_mobile, receiver_address: receiver_address, delivery_type: delivery_type, payment_method: payment_type, sub_total: "$" + " " + totalString, discount: "- AED " + discountStr, total: "$" + " " + total, shipper_image: shipper_image, shipper_name: shipper_name, shipper_rating: customer_rating, shipper_vehicle_number: shipper_vehicle_number,sender_image:sender_image,sender_name: sender_name,sender_lat: "",sender_long: "",receiver_lat: "",receiver_long: "",sender_mobile_code: "",sender_mobile: ""))
                        
                    }
                    
                    self.customTable.reloadData()
                }
                
            }
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
            
        }
    }


    
    @IBAction func pastOrder(_ sender: Any) {
        tabString = "past"
        pastImg.image = UIImage (named: "ellipse25")
        scheduleImg.image = nil
        customTable.reloadData()
    }
    
    @IBAction func scheduleOrder(_ sender: Any) {
        tabString = "current"
        pastImg.image = nil
        scheduleImg.image = UIImage (named: "ellipse25")
        customTable.reloadData()
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        customView.isHidden = true
    }
    
    @IBAction func addBtnClicked(_ sender: Any) {
        customView.isHidden = true
    }
   
    
    @IBAction func dontSend(_ sender: Any) {

        send.setImage(UIImage (named: "radioselect"), for: .normal)
        tooFar.setImage(UIImage (named: "radio"), for: .normal)
        mistake.setImage(UIImage (named: "radio"), for: .normal)
        other.setImage(UIImage (named: "radio"), for: .normal)

        messageTxt.isHidden = true
        cancelHeight.constant = 420
        
    }
    
    @IBAction func tooFar(_ sender: Any) {
        
        send.setImage(UIImage (named: "radio"), for: .normal)
        tooFar.setImage(UIImage (named: "radioselect"), for: .normal)
        mistake.setImage(UIImage (named: "radio"), for: .normal)
        other.setImage(UIImage (named: "radio"), for: .normal)
        
        messageTxt.isHidden = true
        cancelHeight.constant = 420
        
    }
    
    @IBAction func mistakeBtn(_ sender: Any) {
        
        send.setImage(UIImage (named: "radio"), for: .normal)
        tooFar.setImage(UIImage (named: "radio"), for: .normal)
        mistake.setImage(UIImage (named: "radioselect"), for: .normal)
        other.setImage(UIImage (named: "radio"), for: .normal)
        
        messageTxt.isHidden = true
        cancelHeight.constant = 420
        
    }
    
    @IBAction func otherBtn(_ sender: Any) {
        
        send.setImage(UIImage (named: "radio"), for: .normal)
        tooFar.setImage(UIImage (named: "radio"), for: .normal)
        mistake.setImage(UIImage (named: "radio"), for: .normal)
        other.setImage(UIImage (named: "radioselect"), for: .normal)
        
        messageTxt.isHidden = false
        cancelHeight.constant = 490
        
    }
}
extension myOrderViewController : UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tabString == "current"{
            return current_orders.count
        }
        else {
            print(past_orders.count)
            return past_orders.count
        }
      
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as! myOrderTableViewCell
        
        cell.pickView.layer.cornerRadius = cell.pickView.frame.height/2
        cell.dropView.layer.cornerRadius = cell.dropView.frame.height/2
        
        
        if tabString == "current" {
            cell.completedLbl.text = ""
            cell.completedIcon.image = nil
            if cell.trackBtnOutlet.isHidden == true{
                cell.trackBtnOutlet.isHidden = false
            }
            
            cell.trackBtnOutlet.setTitle(orderArr[indexPath.row].localized, for: .normal)
            
            cell.trackBtnOutlet.backgroundColor = UIColor.clear
  
            cell.order_id.text = "ORDER ID:".localized + " \(current_orders[indexPath.row].order_id)"
            cell.amount.text = "\(current_orders[indexPath.row].total)"
            cell.pickUp.text = current_orders[indexPath.row].pickUp
            cell.drop.text = current_orders[indexPath.row].receiver_address
            cell.dateTime.text = current_orders[indexPath.row].date
            
        }
        else{
            if cell.trackBtnOutlet.isHidden == false{
                cell.trackBtnOutlet.isHidden = true
            }
//            print(orderArr[indexPath.row])
            if past_orderArr[indexPath.row] == "completed"{
                cell.completedLbl.text = "Completed".localized
                cell.completedIcon.image = UIImage (named: "completed")
                cell.completedLbl.textColor = UIColor().HexToColor(hexString: "18D293")
            }
            else{
                cell.completedLbl.text = "Cancelled".localized
                cell.completedIcon.image = UIImage (named: "cancel")
                cell.completedLbl.textColor = UIColor().HexToColor(hexString: "db2f2f")
            }
            
            
            
            
            cell.order_id.text = "ORDER ID:".localized + " \(past_orders[indexPath.row].order_id)"
            cell.amount.text = "\(past_orders[indexPath.row].total)"
            cell.pickUp.text = past_orders[indexPath.row].pickUp
            cell.drop.text = past_orders[indexPath.row].receiver_address
            cell.dateTime.text = past_orders[indexPath.row].date
        }
        
      
        cell.selectionStyle = .none
        cell.trackBtnOutlet.layer.cornerRadius = cell.trackBtnOutlet.frame.height/2
        return cell
    }
    
    @objc func buttonPressed(_ sender: UIButton) {
        if tabString == "schedule"{
            messageTxt.text = ""
            send.setImage(UIImage (named: "radioselect"), for: .normal)
            tooFar.setImage(UIImage (named: "radio"), for: .normal)
            mistake.setImage(UIImage (named: "radio"), for: .normal)
            other.setImage(UIImage (named: "radio"), for: .normal)
            
            messageTxt.isHidden = true
            cancelHeight.constant = 420
            
            customView.isHidden = false
            self.customView.alpha = 0.0
            UIView.animate(withDuration: 1.0, delay: 0, options: .curveEaseInOut, animations: {
                self.customView.alpha = 1.0
            })
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        current_Index = indexPath.row
        itemList_send.removeAll()
        
        
        if tabString == "past"{
            itemList_send = groups_past[indexPath.row]
            orderStatus = past_orderArr[indexPath.row]
            print(past_orderArr)
             self.performSegue(withIdentifier: "detail", sender: nil)
        }
        else{
            itemList_send = groups[indexPath.row]
            orderStatus = orderArr[indexPath.row]
            
            print(orderStatus)
            
            self.performSegue(withIdentifier: "home", sender: nil)
        }
        
//        if tabString == "schedule"{
//            self.performSegue(withIdentifier: "home", sender: nil)
//
//        }
//        else{
//            self.performSegue(withIdentifier: "detail", sender: nil)
//        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail"{
            let Object = segue.destination as! orderDetailViewController
            Object.StateStr = tabString
            Object.orderStatus = orderStatus
            
            Object.Index = current_Index
            Object.itemList = itemList_send
            
            
            if tabString == "current"{
                Object.orders = current_orders
            }
            else if tabString == "past"{
                Object.orders = past_orders
            }
        
        }
        else if segue.identifier == "home"{
            let Object = segue.destination as! HomeViewController
            Object.Index = current_Index
            Object.newItemList = itemList_send
            Object.orders = current_orders
            print(orderStatus)
            Object.order_status = orderStatus
            Object.flag = "status"
            
        }
    }
    
}


extension Float
{
    var cleanValue: String
    {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}


extension Notification.Name {
    static let cart = Notification.Name("cartAdd")
}
