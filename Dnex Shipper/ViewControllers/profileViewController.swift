//
//  profileViewController.swift
//  Dnex Shipper
//
//  Created by Arka on 17/03/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

class profileViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var backImg: UIImageView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var frontImg: UIImageView!
    @IBOutlet weak var sideBtn: UIButton!
    
    @IBOutlet weak var NameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var mobileLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(getProfile), name: .getProfile, object: nil)

        
        headerView.setShadow()
        
        
        backImg.layer.cornerRadius = 20
        backView.layer.cornerRadius = 20
        
        frontImg.layer.cornerRadius = frontImg.frame.height/2
        frontImg.layer.borderWidth = 5
        frontImg.layer.borderColor = UIColor().HexToColor(hexString: "78D5E8", alpha: 1.0).cgColor
        
        
     
        
        NetworkManager.getProfile(uiRef: self, Webservice: "get-profile"){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            
            if sJSON["status"].intValue == 200{
                
                self.NameLbl.text = sJSON["data"]["name"].stringValue
                self.emailLbl.text = sJSON["data"]["email"].stringValue
                self.mobileLbl.text = sJSON["data"]["country_code"].stringValue + "-" + sJSON["data"]["mobile"].stringValue
                
                let image = "http://103.207.168.164:8008/storage/" + sJSON["data"]["profile_pic"].stringValue
                self.backImg.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "defaultProfile"))
                self.frontImg.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "defaultProfile"))
                
            }
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
        }
        
    }
    
    
      @objc func getProfile(){
        
//            GIFHUD.shared.show(withOverlay: true, duration: 2)
        NetworkManager.getProfile(uiRef: self, Webservice: "get-profile"){sJson in
            GIFHUD.shared.dismiss()
                
            let sJSON = JSON(sJson)
            print(sJSON)
            
            if sJSON["status"].intValue == 200{
                
                self.NameLbl.text = sJSON["data"]["name"].stringValue
                self.emailLbl.text = sJSON["data"]["email"].stringValue
                self.mobileLbl.text = sJSON["data"]["country_code"].stringValue + "-" + sJSON["data"]["mobile"].stringValue
                
                let image = "http://103.207.168.164:8008/storage/" + sJSON["data"]["profile_pic"].stringValue
                
                self.backImg.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "defaultProfile"))
                self.frontImg.sd_setImage(with: URL(string: image), placeholderImage: UIImage(named: "defaultProfile"))
                
            }
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "edit"{
            
            let Object = segue.destination as! editProfileViewController
            Object.nameStr = self.NameLbl.text!
            Object.emailStr = self.emailLbl.text!
            
            let mobile = mobileLbl.text?.components(separatedBy: "-")
            Object.codeStr = mobile![0]
            Object.mobileStr = mobile![1]
            Object.profile = frontImg.image!
            
        }
    }
    
}
