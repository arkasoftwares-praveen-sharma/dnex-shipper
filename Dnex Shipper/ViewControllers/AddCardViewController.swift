//
//  AddCardViewController.swift
//  Dnex
//
//  Created by Arka on 08/01/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddCardViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var addBtnOutlet: UIButton!
    
    @IBOutlet weak var cardNumber: ACFloatingTextfield!
    @IBOutlet weak var CardHolderName: ACFloatingTextfield!
    @IBOutlet weak var IFSCCode: ACFloatingTextfield!
    @IBOutlet weak var bankName: ACFloatingTextfield!
    @IBOutlet weak var sideBtn: UIButton!
    
   
    var bank_id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        headerView.setShadow()
        
         addBtnOutlet.layer.cornerRadius = addBtnOutlet.frame.height/2
         GIFHUD.shared.setGif(named: "loader.gif")
        cardNumber.delegate = self
        CardHolderName.delegate = self
        
        
        let home = UserDefaults.standard.string(forKey: "sideIcon") ?? ""
        
        if home == ""{
            sideBtn.isHidden = true
        }
        else{
            sideBtn.isHidden = false
        }
        
        NetworkManager.getProfile(uiRef: self, Webservice: "get-profile"){sJson in
            
            let sJSON = JSON(sJson)
            print(sJSON)
            
            if sJSON["status"].intValue == 200{
              
                self.cardNumber.text = sJSON["data"]["get_bank_details"]["account_number"].stringValue
                self.bankName.text = sJSON["data"]["get_bank_details"]["bank_name"].stringValue
                self.CardHolderName.text = sJSON["data"]["get_bank_details"]["account_holder_name"].stringValue
                self.IFSCCode.text = sJSON["data"]["get_bank_details"]["iban_number"].stringValue
                
                self.addBtnOutlet.setTitle("Update".localized, for: .normal)
                self.bank_id = sJSON["data"]["get_bank_details"]["id"].stringValue
            }
            else{
                CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
            }
        }
    }

  
    @IBAction func addBtnClicked(_ sender: Any) {
        
        
        if bankName.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter bank's name !!".localized)
        }
        else if CardHolderName.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter account holder's name !!".localized)
        }
        else if cardNumber.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter account number !!".localized)
        }
        else if IFSCCode.text == ""{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Enter IFSC !!".localized)
        }
            
        else{
            GIFHUD.shared.show(withOverlay: true, duration: 2)
            if bank_id != ""{
                
                NetworkManager.updateBankDetails(uiRef: self, Webservice: "update-shipper-bank-detail",bank_details_id: bank_id, bank_name: bankName.text!, account_holder_name: CardHolderName.text!, account_number: cardNumber.text!, iban_number: IFSCCode.text!){sJson in
                    GIFHUD.shared.dismiss()
                    let sJSON = JSON(sJson)
                    
                    print(sJSON)
                    
                    if sJSON["status"].intValue == 200{
                        self.showAlert(message: "Bank details updated successfully !!".localized)
                    }
                    else{
                        CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                    }
                }
            }
            else{
                
                NetworkManager.addBankDetails(uiRef: self, Webservice: "add-shipper-bank-detail", bank_name: bankName.text!, account_holder_name: CardHolderName.text!, account_number: cardNumber.text!, iban_number: IFSCCode.text!){sJson in
                    GIFHUD.shared.dismiss()
                    let sJSON = JSON(sJson)
                    
                    print(sJSON)
                    
                    if sJSON["status"].intValue == 200{
                        self.showAlert(message: "Admin will verify the account and then you will be able to receive new orders.".localized)
                    }
                    else{
                        CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                    }
                }
            }
        }
    }
    
    
    func showAlert( message: String) {
        let alertController = UIAlertController(title: "Message".localized, message:
              message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: {action in
              
             self.performSegue(withIdentifier: "success", sender: nil)
           
          }))
                   
          self.present(alertController, animated: true, completion: nil)
          
      }
}
