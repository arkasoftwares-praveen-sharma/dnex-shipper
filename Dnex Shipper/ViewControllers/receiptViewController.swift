//
//  receiptViewController.swift
//  Dnex Shipper
//
//  Created by Arka on 03/03/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import SDWebImage
import Cosmos
import SwiftyJSON



class receiptViewController: UIViewController {

    @IBOutlet weak var feedbackTxt: ACFloatingTextfield!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var pickUpMain: UIView!
    @IBOutlet weak var pickView: UIView!
    
    @IBOutlet weak var rateBtnOutlet: UIButton!
    @IBOutlet weak var initialLbl: UILabel!
    @IBOutlet weak var doneBtnOutlet: UIButton!
    @IBOutlet weak var starView: UIView!
    @IBOutlet weak var submitBtnOutlet: UIButton!
    @IBOutlet weak var customView: UIView!
    
    
    @IBOutlet weak var order_id: UILabel!
    @IBOutlet weak var pickUp: UILabel!
    @IBOutlet weak var senderImg: UIImageView!
    
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var senderRating: UIButton!
    
    @IBOutlet weak var receiverName: UILabel!
    
    @IBOutlet weak var receiverInitials: UILabel!
    
    @IBOutlet weak var receiverMobile: UILabel!
    
    @IBOutlet weak var receiverAddress: UILabel!
    
    @IBOutlet weak var deliveryType: UILabel!
    
    @IBOutlet weak var paymentMode: UILabel!
    
    @IBOutlet weak var subTotal: UILabel!
    
    
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var starRate: CosmosView!
    
    
    @IBOutlet weak var ratingTitle: UILabel!
    
    @IBOutlet weak var Note: UILabel!
    
    @IBOutlet weak var noteLbl: UILabel!
    
    
    
    var Index = 0
    var orders:[current] = []
    var itemList:[item_order] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        heightConstraint.constant = 1080
        
//        itemList.append(item.init(name: "chair", description: "Test description for chair", height: "20", weight: "10", width: "2", item_image: UIImage (named: "noPathCopy2")!))
//        itemList.append(item.init(name: "Table", description: "Test description for table", height: "20", weight: "10", width: "2", item_image: UIImage (named: "noPathCopy2")!))
//        itemList.append(item.init(name: "chair", description: "Test description for chair", height: "20", weight: "10", width: "2", item_image: UIImage (named: "noPathCopy2")!))
        
        
        
        senderImg.layer.cornerRadius = senderImg.frame.height/2
        heightConstraint.constant = CGFloat(850 + (130 * itemList.count))
        
        
        pickUpMain.layer.cornerRadius = pickUpMain.frame.height/2
        pickUpMain.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        pickUpMain.layer.borderWidth = 1
        
        
        pickView.layer.cornerRadius = pickView.frame.height/2
        rateBtnOutlet.layer.cornerRadius = rateBtnOutlet.frame.height/2
        doneBtnOutlet.layer.cornerRadius = doneBtnOutlet.frame.height/2
        
        submitBtnOutlet.layer.cornerRadius = submitBtnOutlet.frame.height/2
        
        initialLbl.layer.cornerRadius = initialLbl.frame.height/2
        initialLbl.clipsToBounds = true
        
        
        starView.layer.cornerRadius = starView.frame.height/2
        starView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        starView.layer.borderWidth = 1
        
        
        
        order_id.text = "Order Id:".localized + "\(orders[Index].order_id)"
        dateTime.text = orders[Index].date
        pickUp.text = orders[Index].pickUp
        senderName.text = orders[Index].sender_name
        let sender_image = orders[Index].sender_image
        self.senderImg.sd_setImage(with: URL(string: sender_image), placeholderImage: UIImage(named: "defaultProfile"))
        receiverName.text = orders[Index].receiver_name
        let initials = receiverName.text!.components(separatedBy: " ").reduce("") { ($0 == "" ? "" : "\($0.first!)") + "\($1.first!)" }
        
        receiverInitials.text = initials
        receiverMobile.text = orders[Index].receiver_mobile_code + orders[Index].receiver_mobile
        receiverAddress.text = orders[Index].receiver_address
        deliveryType.text = orders[Index].delivery_type
        paymentMode.text = orders[Index].payment_method
        subTotal.text = orders[Index].sub_total
        total.text = orders[Index].total
        
        let sender_name = orders[Index].sender_name
        self.ratingTitle.text = "Tell us how was your experience with".localized + " \(sender_name)"
        
        
        if orders[Index].type == "sell"{
            Note.isHidden = false
            noteLbl.isHidden = false
            
            noteLbl.text = "Please collect".localized + " \(orders[Index].order_type) " + "from receiver".localized
        }
        else{
            Note.isHidden = true
            noteLbl.isHidden = true
        }
    }

    
    
    @IBAction func doneBtnClicked(_ sender: Any) {
        
        self.performSegue(withIdentifier: "myOrder", sender: nil)
        
    }
    
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        
        if starRate.rating == 0{
            CommonFunctions.showErrorAlert(uiRef: self, message: "Please provide a rating to the customer !!".localized)
        }
        else{
            let order_id = orders[Index].id
            let sender_id = orders[Index].sender_id
            let rating = String(starRate.rating)
            let feedback = feedbackTxt.text ?? ""
            
            NetworkManager.rating(uiRef: self, Webservice: "add-rating", order_id: order_id, shipper_id: sender_id, rating: rating, feedback: feedback){sJson in
                let sJSON = JSON(sJson)
                print(sJSON)
                if sJSON["status"].intValue == 200{
                    self.customView.alpha = 1.0
                    UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                        self.customView.alpha = 0.0
                    })
                }
                else{
                    CommonFunctions.showErrorAlert(uiRef: self, message: sJSON["message"].stringValue)
                }
            }
        }
    }
    

    @IBAction func closeRate(_ sender: Any) {
        
//         customView.isHidden = true
        
        self.customView.alpha = 1.0
        //          forgotEmailTxt.text = ""
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.customView.alpha = 0.0
        })
    }
}

extension receiptViewController:UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CELL") as! ItemListTableViewCell
        
        cell.itemView.layer.cornerRadius = 10
        cell.itemView.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        cell.itemView.layer.borderWidth = 1
        
        cell.itemImage.layer.cornerRadius = 10
        cell.itemImage.layer.borderColor = UIColor().HexToColor(hexString: "97a8ab", alpha: 0.5).cgColor
        cell.itemImage.layer.borderWidth = 1
        
        cell.itemName.text = itemList[indexPath.row].name
        cell.itemDesc.text = itemList[indexPath.row].description
        cell.height.text = itemList[indexPath.row].height
        cell.width.text = itemList[indexPath.row].width
        cell.weight.text = itemList[indexPath.row].weight
        
//        cell.itemImage.image = itemList[indexPath.row].item_image
          cell.itemImage.sd_setImage(with: URL(string: itemList[indexPath.row].item_image), placeholderImage: UIImage(named: "item_placeholder"))
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 124
        
    }
    
}
