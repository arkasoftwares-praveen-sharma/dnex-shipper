//
//  cmsViewController.swift
//  Dnex
//
//  Created by Praveen Sharma on 15/04/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit
import WebKit

class cmsViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        GIFHUD.shared.setGif(named: "loader.gif")
       
        let title = UserDefaults.standard.string(forKey: "title")
        titleLbl.text = title
        
        UserDefaults.standard.removeObject(forKey: "title")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        GIFHUD.shared.show(withOverlay: true, duration: 2)
        
        let link = UserDefaults.standard.string(forKey: "link") ?? ""
        UserDefaults.standard.removeObject(forKey: "link")
        let myURLString = link
        let url = URL(string: myURLString)
        let request = URLRequest(url: url!)
        
        
        
        webView.navigationDelegate = self
        webView.load(request)
    }
}

extension cmsViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Started to load")
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished loading")
        
        GIFHUD.shared.dismiss()
        
    }

    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print(error.localizedDescription)
    }
}
