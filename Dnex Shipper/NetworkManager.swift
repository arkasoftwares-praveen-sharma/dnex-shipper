//
//  NetworkManager.swift
//  Currency Converter
//
//  Created by Arka Softwares on 12/12/19.
//  Copyright © 2019 Arka Softwares. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

// import Reachability

class NetworkManager:NSObject {
    
    
    var ResponseStr:String = ""
    static var TokenDict = [String: String]()
    var dictionaryForStatus:[String] = []
    
    
    static let PROTOCOL:String = "http://";
    //    103.207.168.164:8004/
    static let SUB_DOMAIN:String = "103.207.168.164:8008";
    
    //    http://103.207.168.164:8008/api/APINAME
    
    static let DOMAIN:String = "/api/";//Production Service End
    static let API_DIR:String = "";
    
    static let SITE_URL = PROTOCOL + SUB_DOMAIN + DOMAIN;
    static var API_URL = SITE_URL + API_DIR;
    
    
    // Call Functions
    
    
    // POST
    
    
    static func callService(uiRef: UIViewController,Webservice: String, parameters:Parameters, completion:@escaping (JSON) -> Void){
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request("\(Webservice)",method:.post, parameters:parameters,encoding:JSONEncoding.default).responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                switch response.result{
                    
                case .failure(let error):
                    print("Error : \(error)" )
                    
                    self.showErrorAlert(uiRef: uiRef, message: "Internet connection appears to be offline")
                    //
                    break
                    
                case .success(_):
                    print("Success")
                    break
                }
                
                
                if let json = response.result.value {
                    print("Output: \(json)") // serialized json response
                    let sJSON = JSON(json)
                    
                    
                    if sJSON["status"].intValue == 200 {
                        // we're OK to parse!
                    }
                    else { // Error found
                        print("ErrorSERVICE: ")
                        //                    sJSON["message"].stringValue)
                        
                        //                    print(sJSON["message"].stringValue)
                        
                        //                    NetworkManager.showErrorAlert(uiRef: uiRef,message:  sJSON["message"].stringValue)
                    }
                    completion (sJSON)
                }
                
                
                //            NetworkManager.showErrorAlert(uiRef: uiRef,message:  "Something went wrong, Please try again")
            }
        }
            
        else {
            GIFHUD.shared.dismiss()
            showErrorAlert(uiRef: uiRef, message: "Please check your internet connection")
        }
    }
    
    
    static func callServiceUpdateEvent(url:String, parameters:Parameters,event_images:Array<Data>, national_id:UIImage? = nil, completion:@escaping (JSON) -> Void){
        
        // let TokenDict:HTTPHeaders  = ["Content-Type": "application/json"]
        var counter = 0
        
//        let tokenToSend:String = UserDefaults.standard.string(forKey: "token") ?? ""
//
//        print(tokenToSend)
//
////        tokenToSend = "Bearer " + tokenToSend
//        TokenDict = ["Authorization" : tokenToSend]
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                if let item:UIImage = national_id{
                    
                    guard let imageData = item.jpegData(compressionQuality: 1.0) else {
                        print("Could not get JPEG representation of UIImage")
                        return
                        
                    }
                    multipartFormData.append(imageData, withName: "document",fileName:"photo.jpg" ,mimeType: "image/jpg")
                }
                
                print(event_images.count)
                
                if let item:Array = event_images {
                    for index in event_images  {
                        print(counter)
                        multipartFormData.append(index, withName: "images[\(counter)]",fileName:"photo.jpg" ,mimeType: "image/jpg")
                        counter = counter+1
                    }
                }
                
                for (key, value) in parameters {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
        }, to:  url,headers: TokenDict) { (encodingResult) in
            switch encodingResult {
            case .success(let upload, _,_):
                upload.responseJSON { response in
                    print("Request: \(String(describing: response.request))")   // original url request
                    print("Response: \(String(describing: response.response))") // http url response
                    print("Result: \(response.result.value)")                         // response serialization result
                    
                    
                    if let json = response.result.value {
                        print("Output: \(json)") // serialized json response
                        let sJSON = JSON(json)
                        
                        
                        if sJSON["status"].intValue == 200 {
                            // we're OK to parse!
                        }
                        else { // Error found
                            print("Error: ")
                            
                        }
                        completion (sJSON)
                    } else{
                        completion("Something went wrong!!")
                        //                        ANLoader.hide()
                    }
                }
            case .failure(let encodingError):
                completion("Something went wrong!!")
                //                ANLoader.hide()
            }
        }
    }
    
    
    static func callServiceTimeline(url:String, parameters:Parameters,profile_pic:UIImage?, completion:@escaping (JSON) -> Void){
        
        //        let TokenDict:HTTPHeaders  = ["Content-Type": "application/json"]
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                //                if let item:UIImage = profile_pic{
                
                //                guard let imageData = profile_pic.jpegData(compressionQuality: 0.5) else {
                //                    print("Could not get JPEG representation of UIImage")
                //                    return
                //
                //                }
                
                
                let imageData = profile_pic?.jpegData(compressionQuality: 0.5)
                
                multipartFormData.append(imageData!, withName: "profile_pic",fileName:"photo.jpg" ,mimeType: "image/jpg")
                
                
                for (key, value) in parameters {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
        }, to:  url, headers:TokenDict) { (encodingResult) in
            switch encodingResult {
            case .success(let upload,_ ,_):
                upload.responseJSON { response in
                    print("Request: \(String(describing: response.request))")   // original url request
                    print("Response: \(String(describing: response.response))") // http url response
                    print("Result: \(response.result.value)")                         // response serialization result
                    
                    
                    if let json = response.result.value {
                        print("Output: \(json)") // serialized json response
                        let sJSON = JSON(json)
                        
                        
                        if sJSON["status"].intValue == 200 {
                            // we're OK to parse!
                        }
                        else { // Error found
                            print("Error: ")
                            //                    sJSON["message"].stringValue)
                            
                            //                    print(sJSON["message"].stringValue)
                            
                            //                    NetworkManager.showErrorAlert(uiRef: uiRef,message:  sJSON["message"].stringValue)
                        }
                        completion (sJSON)
                    }     else {
                    }
                    
                }
            case .failure(let encodingError):
                completion("Something went wrong!!")
            }
        }
    }
    
    
    
    
    static func callServiceVerify(uiRef: UIViewController,Webservice: String, parameters:Parameters, completion:@escaping (JSON) -> Void){
        if Reachability.isConnectedToNetwork(){
            Alamofire.request("\(Webservice)",method:.post, parameters:parameters,encoding:URLEncoding.default,headers: TokenDict).responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                
                if let json = response.result.value {
                    print("Output: \(json)") // serialized json response
                    let sJSON = JSON(json)
                    
                    
                    if sJSON["status_code"].intValue == 200 {
                        // we're OK to parse!
                    }
                    else { // Error found
                        print("Error: ")
                        //                                        sJSON["message"].stringValue)
                        //
                        //                                        print(sJSON["message"].stringValue)
                        
                        //                    NetworkManager.showErrorAlert(uiRef: uiRef,message:  sJSON["message"].stringValue)
                    }
                    completion (sJSON)
                }
                else{
                    GIFHUD.shared.dismiss()
                    
                    //              NetworkManager.showErrorAlert(uiRef: uiRef,message:  "Something went wrong, Please try again")
                }
                //            NetworkManager.showErrorAlert(uiRef: uiRef,message:  "Something went wrong, Please try again")
            }
        }
        else {
            GIFHUD.shared.dismiss()
            showErrorAlert(uiRef: uiRef, message: "Please check your internet connection")
        }
    }
    
    
    static func callServiceNew(Webservice: String, parameters:Parameters, completion:@escaping (JSON) -> Void){
        if Reachability.isConnectedToNetwork(){
            Alamofire.request("\(Webservice)",method:.post, parameters:parameters,encoding:URLEncoding.default,headers: TokenDict).responseJSON { response in
                print("Request: \(String(describing: response.request))")   // original url request
                print("Response: \(String(describing: response.response))") // http url response
                print("Result: \(response.result)")                         // response serialization result
                
                
                if let json = response.result.value {
                    print("Output: \(json)") // serialized json response
                    let sJSON = JSON(json)
                    
                    
                    if sJSON["status_code"].intValue == 200 {
                        // we're OK to parse!
                    }
                    else { // Error found
                        print("Error: ")
                        //                                        sJSON["message"].stringValue)
                        //
                        //                                        print(sJSON["message"].stringValue)
                        
                        //                    NetworkManager.showErrorAlert(uiRef: uiRef,message:  sJSON["message"].stringValue)
                    }
                    completion (sJSON)
                }
                else{
                    
                    //              NetworkManager.showErrorAlert(uiRef: uiRef,message:  "Something went wrong, Please try again")
                }
                //            NetworkManager.showErrorAlert(uiRef: uiRef,message:  "Something went wrong, Please try again")
            }
        }
        else {
            GIFHUD.shared.dismiss()
            //               showErrorAlert(uiRef: uiRef, message: "Please check your internet connection")
        }
    }
    
    
    // GET
    
    static func callServiceGet(uiRef: UIViewController,Webservice: String, parameters:Parameters, completion:@escaping (JSON) -> Void){
        
        print(TokenDict)
        
        if Reachability.isConnectedToNetwork(){
            
            Alamofire.request("\(Webservice)", method: .get, parameters: parameters, encoding: URLEncoding.default, headers: TokenDict).responseJSON
                {
                    response in
                    print("Request: \(String(describing: response.request))")   // original url request
                    print("Response: \(String(describing: response.response))") // http url response
                    print("Result: \(response.result)")                         // response serialization result
                    
                    switch(response.result)
                    {
                    case .success(let value): break
                    // completionHandler(value as? NSDictionary, nil)
                    case .failure(let error as NSError):
                        //  completionHandler(nil, error as NSError?)
                        print(error)
                        
                        
                        break
                    }
                    
                    if let json = response.result.value {
                        print("Output: \(json)") // serialized json response
                        let sJSON = JSON(json)
                        
                        
                        if sJSON["status"].intValue == 200 {
                            // we're OK to parse!
                        }
                        else { // Error found
                            print("Error: ")
                            //                    sJSON["message"].stringValue)
                            
                            //                    print(sJSON["message"].stringValue)
                            
                            //  NetworkManager.showErrorAlert(uiRef: uiRef,message:  sJSON["message"].stringValue)
                        }
                        
                        completion (sJSON)
                    }
            }
        }
        else {
            
            GIFHUD.shared.dismiss()
            showErrorAlert(uiRef: uiRef, message: "Please check your internet connection")
        }
    }
    
    static func login(uiRef: UIViewController,Webservice: String,country_code:String,mobile:String,password:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        let parameters: Parameters = ["country_code":country_code,"mobile":mobile,"password":password,"user_type":"3"]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    
    static func sendOTP(uiRef: UIViewController,Webservice: String,mobile: String,country_code:String,otp_type:String,email:String,user_type:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        
        let parameters: Parameters = [
            "mobile": mobile,
            "country_code":country_code,
            "otp_type" : otp_type,
            "email" : email,
            "user_type" : user_type
        ]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callService(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func resetPassword(uiRef: UIViewController,Webservice: String,mobile: String,country_code:String,otp:String,password:String,created_at:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        
        let parameters: Parameters = [
            "mobile": mobile,
            "country_code":country_code,
            "otp" : otp,
            "password" : password,
            "created_at" : created_at
        ]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callService(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func verifyOTP(uiRef: UIViewController,Webservice: String,mobile: String,country_code:String,otp_type:String,otp:String,created_at:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        
        let parameters: Parameters = [
            "mobile": mobile,
            "country_code":country_code,
            "otp_type" : otp_type,
            "otp" : otp,
            "created_at" : created_at
        ]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callService(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func signUp(uiRef: UIViewController,Webservice: String,mobile: String,country_code:String,name:String,email:String,password:String,user_type:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        
        let parameters: Parameters = [
            "mobile": mobile,
            "country_code":country_code,
            "email" : email,
            "password" : password,
            "user_type" : user_type,
            "name" : name
        ]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callService(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func getProfile(uiRef: UIViewController,Webservice: String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        let parameters: Parameters = [:]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func addBankDetails(uiRef: UIViewController,Webservice: String,bank_name: String,account_holder_name:String,account_number:String,iban_number:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        let parameters: Parameters = [
            "bank_name": bank_name,
            "account_holder_name":account_holder_name,
            "account_number" : account_number,
            "iban_number" : iban_number
        ]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func updateBankDetails(uiRef: UIViewController,Webservice: String,bank_details_id:String,bank_name: String,account_holder_name:String,account_number:String,iban_number:String,completion: @escaping (JSON) -> Void) {
         
         let API_Call = API_URL + Webservice
         let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
         
         print(tokenToSend)
         
         TokenDict = ["Authorization": tokenToSend]
         let parameters: Parameters = [
            "bank_details_id": bank_details_id,
            "bank_name": bank_name,
             "account_holder_name":account_holder_name,
             "account_number" : account_number,
             "iban_number" : iban_number
         ]
         
         print(API_Call)
         print(parameters)
         
         NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
             completion(JSON)
         }
     }
    
    
    static func onlineStatus(uiRef: UIViewController,Webservice: String,online_status: String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        let parameters: Parameters = [
            "online_status": online_status
        ]
        
        print(API_Call)
        print(parameters)
        
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func checkOnlineStatus(uiRef: UIViewController,Webservice: String,completion: @escaping (JSON) -> Void) {
           
           let API_Call = API_URL + Webservice
           
           let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
           
           print(tokenToSend)
           
           TokenDict = ["Authorization": tokenToSend]
           
           let parameters: Parameters = [:]
           
           print(API_Call)
           print(parameters)
           
           NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
               completion(JSON)
           }
       }
    
    
    static func accept(uiRef: UIViewController,Webservice: String,order_id: String,status: String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        let parameters: Parameters = [
            "order_id": order_id,
            "status" : status
        ]
        
        print(API_Call)
        print(parameters)
        
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func shareLocation(uiRef: UIViewController,Webservice: String,latitude: Double,longitude: Double,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        let parameters: Parameters = [
            "latitude" : latitude,
            "longitude" : longitude
        ]
        
        print(API_Call)
        print(parameters)
        
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    static func getOrders(uiRef: UIViewController,Webservice: String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        let parameters: Parameters = [:]
        
        print(API_Call)
        print(parameters)
        
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    static func EditProfile(webservice:String,email:String,media:UIImage?,name:String,country_code:String,mobile:String, completion: @escaping (Any) -> Void) {
        
        
        let API_Call = API_URL + webservice
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        
        let parameters: Parameters = [
            "name": name,
            "email" : email,
            "country_code" : country_code,
            "mobile" : mobile
        ]
        
        print(parameters)
        
        
        NetworkManager.callServiceTimeline(url: API_Call, parameters: parameters, profile_pic: media) { (JSON) in
            completion(JSON)
        }
    }
    
    static func vehicleDetail(webservice:String,vehicle_no:String,capacity:String,vehicle_images:Array<Data>,national_id:UIImage, completion: @escaping (Any) -> Void) {
        
        
        let API_Call = API_URL + webservice
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token") ?? ""
        
//        tokenToSend = "Bearer " + tokenToSend
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        let parameters: Parameters = [
            "vehicle_no": vehicle_no,
            "capacity" : capacity
        ]
        
        print(parameters)
        
        NetworkManager.callServiceUpdateEvent(url: API_Call, parameters:parameters, event_images: vehicle_images,national_id: national_id)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func changePassword(uiRef: UIViewController,Webservice: String,old_password:String,new_password:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        let parameters: Parameters = ["old_password":old_password,
                                      "new_password":new_password]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    
    static func updateStatus(uiRef: UIViewController,Webservice: String,order_id:String,order_status:Int,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        let parameters: Parameters = ["order_id":order_id,
                                      "order_status":order_status]
        
        print(API_Call)
        print(parameters)
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    static func update_device_id(uiRef: UIViewController,Webservice:String,device_token:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        let parameters: Parameters = ["device_token":device_token]
        
        print(API_Call)
        print(parameters)
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    static func pass_order(uiRef: UIViewController,Webservice:String,mobile:String,order_id:String,completion: @escaping (JSON) -> Void) {
        
        let API_Call = API_URL + Webservice
        let parameters: Parameters = ["mobile":mobile,"order_id":order_id]
        
        print(API_Call)
        print(parameters)
        
        let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
        
        print(tokenToSend)
        
        TokenDict = ["Authorization": tokenToSend]
        
        NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
            completion(JSON)
        }
    }
    
    static func rating(uiRef: UIViewController,Webservice: String,order_id:String,shipper_id:String,rating:String,feedback:String,completion: @escaping (JSON) -> Void) {
          
          let API_Call = API_URL + Webservice
          let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
          
          print(tokenToSend)
          
          TokenDict = ["Authorization": tokenToSend]
          
          let parameters: Parameters = ["order_id":order_id,
                                        "shipper_id":shipper_id, "rating":rating,"feedback":feedback]
          
          print(API_Call)
          print(parameters)
          
          NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
              completion(JSON)
          }
      }
    
    
    static func myOrders(uiRef: UIViewController,Webservice: String,completion: @escaping (JSON) -> Void) {
             
             let API_Call = API_URL + Webservice
             
             let tokenToSend:String = UserDefaults.standard.string(forKey: "token")!
             
             print(tokenToSend)
             
             TokenDict = ["Authorization": tokenToSend]
             
             
             let parameters: Parameters = [:]
             
             print(API_Call)
             print(parameters)
             
             NetworkManager.callServiceVerify(uiRef:uiRef,Webservice: API_Call, parameters:parameters)  { (JSON) in
                 completion(JSON)
             }
         }
    
    static func showErrorAlert(uiRef: UIViewController, message :String){
        //Alert Message Code
        let alert = UIAlertController(title: "Message", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {action in
            
            
            
        }))
        
        
        uiRef.present(alert, animated: true, completion: nil)
        //End Alert Message Code
    }
}
