//
//  OtherInfoCollectionViewCell.swift
//  Dnex Shipper
//
//  Created by Arka on 26/02/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit

class OtherInfoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var customImage: UIImageView!
    @IBOutlet weak var deletBtn: UIButton!
    
}
