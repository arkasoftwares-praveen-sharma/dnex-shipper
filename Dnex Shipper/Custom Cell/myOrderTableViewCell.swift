//
//  myOrderTableViewCell.swift
//  Dnex
//
//  Created by Arka on 04/03/20.
//  Copyright © 2020 Arka. All rights reserved.
//

import UIKit

class myOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var pickView: UIView!
    @IBOutlet weak var dropView: UIView!
    @IBOutlet weak var trackBtnOutlet: UIButton!
    @IBOutlet weak var completedIcon: UIImageView!
    @IBOutlet weak var completedLbl: UILabel!
    
    
    
    @IBOutlet weak var order_id: UILabel!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var pickUp: UILabel!
    @IBOutlet weak var drop: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
